(*
 * 以下のようにして動作確認
 * ただしnon-exhaustive patternの使用による警告が出る
 *   % ocaml report.ml
 *)

(* 準備 *)

let id x = x
let compose f g x = f (g x)
let flip f x y = f y x
let cons x xs = x :: xs

(* 問1 *)

let rec sum_to = function
  | 0 -> 0
  | n -> n + sum_to (n - 1)

let rec gcd n = function
  | 0 -> n
  | m -> gcd m (n mod m)

let rec is_prime = function
  | 1 -> false
  | n -> let rec iter m =
           if m * m > n
             then true
             else
               if n mod m = 0
                 then false
                 else iter (m + 1) in
          iter 2

(* 動作例 *)
let _ = assert (sum_to 5 = 15)
let _ = assert (gcd 32 48 = 16)
let _ = assert (is_prime 71 = true)
let _ = assert (is_prime 68 = false)

(* 問3 *)

let twice f = compose f f 

let rec repeat f = function
  | 0 -> id
  | n -> compose f (repeat f (n - 1))

(* 動作例 *)
let inc x = x + 1
let _ = assert (twice inc 10 = 12)
let _ = assert (repeat inc 5 10 = 15)

(* 問2 *)

(*
 * 繰り返しが指数のfib
 * 引数が高々2小さくなる再帰呼び出しを二度行うので
 * 引数がkならば少なくとも合わせて2**(k/2+1)-1回の
 * 呼び出しが起こる
 *)
let rec fib1 = function
  | 0 -> 0
  | 1 -> 1
  | n -> fib1 (n - 1) + fib1 (n - 2)

(*
 * 繰り返しが線形のfib
 * repeatにおいて引数が1小さくなる再帰呼び出しを行うので
 * 引数がkならばk+1回のrepeatの呼び出しが起こる
 *)
let fib2 n =
  let (r, _) = repeat (fun (n, m) -> (m, n + m)) n (0, 1) in r

(* 動作例 *)
let _ = assert (fib1 10 = 55)
let _ = assert (fib2 10 = 55)

(* 問4 *)

let rec fix f x = f (fix f) x

(* 動作例 *)
let fact =
  fix (fun f n ->
         if n = 1 then 1 else n * f (n - 1))
let _ = assert (fact 10 = 3628800)

(* 問5 *)

let rec fold_right f xs init = match xs with
  | [] -> init
  | x :: xs -> f x (fold_right f xs init)

let rec fold_left f init = function
  | [] -> init
  | x :: xs -> fold_left f (f init x) xs

(* 動作例 *)
let _ = assert (fold_right cons [1; 2; 3] [] = [1; 2; 3])
let _ = assert (fold_left (flip cons) [] [1; 2; 3] = [3; 2; 1])

(* 問6 *)

let rec append xs ys = match xs with
  | [] -> ys
  | x :: xs -> x :: append xs ys

let rec filter p = function
  | [] -> []
  | x :: xs -> if p x then x :: filter p xs else filter p xs

let rec split = function
  | [] -> ([], [])
  | (x, y) :: l -> let (xs, ys) = split l in (x :: xs, y :: ys)

(* 動作例 *)
let _ = assert (append [1; 2; 3] [4; 5] = [1; 2; 3; 4; 5])
let _ = assert (filter is_prime [1; 2; 3; 4; 5] = [2; 3; 5])
let _ = assert (split [(1, 2); (3, 4)] = ([1; 3], [2; 4]))

(* 問7 *)

let reverse xs = fold_left (flip cons) [] xs

(* 動作例 *)
let _ = assert (reverse [1; 2; 3] = [3; 2; 1])

(* 問8 *)

(*
 * fold_rightで書く方がリストの構造にしたがって素直に書ける
 * fold_leftではどこかでリストを逆順にする必要がある
 * fold_leftで書いたものは末尾再帰で動作する
 *)
let append1 xs ys = fold_right cons xs ys
let append2 xs ys = fold_left (flip cons) ys (reverse xs)

let filter1 p xs =
  fold_right (fun x xs -> if p x then x :: xs else xs) xs []
let filter2 p xs =
  reverse (fold_left (fun xs x -> if p x then x :: xs else xs) [] xs)

let split1 l =
  fold_right (fun (x, y) (xs, ys) -> (x :: xs, y :: ys)) l ([], [])
let split2 l =
  fold_left (fun (xs, ys) (x, y) -> (x :: xs, y :: ys)) ([], []) (reverse l)

(* 動作例 *)
let _ = assert (append1 [1; 2; 3] [4; 5] = [1; 2; 3; 4; 5])
let _ = assert (append2 [1; 2; 3] [4; 5] = [1; 2; 3; 4; 5])
let _ = assert (filter1 is_prime [1; 2; 3; 4; 5] = [2; 3; 5])
let _ = assert (filter2 is_prime [1; 2; 3; 4; 5] = [2; 3; 5])
let _ = assert (split1 [(1, 2); (3, 4)] = ([1; 3], [2; 4]))
let _ = assert (split2 [(1, 2); (3, 4)] = ([1; 3], [2; 4]))

(* 問9 *)

let map f xs = fold_right (compose cons f) xs [] 
let concat xss = fold_right append xss []
let rec zip xs ys = match (xs, ys) with
  | (x :: xs, y :: ys) -> (x, y) :: zip xs ys
  | _ -> []
let rec tails xs = xs :: match xs with [] -> [] | _ :: xs -> tails xs
let rec init = function
  | _ :: [] -> []
  | x :: xs -> x :: init xs
let rec inits xs =
  let inits' = fold_left (fun (xs :: xss) x ->
                            (x :: xs) :: xs :: xss) [[]] xs in
  reverse (map reverse inits')

let rec perm = function
  | [] -> [[]]
  | xs -> (concat (map (fun (xs, y :: ys) ->
                          map (cons y) (perm (append xs ys)))
                       (init (zip (inits xs) (tails xs)))))

(* 動作例 *)
let _ = assert (perm [1] = [[1]])
let _ = assert (perm [1; 2] = [[1; 2]; [2; 1]])
let _ = assert (perm [1; 2; 3] = [[1; 2; 3]; [1; 3; 2];
                                  [2; 1; 3]; [2; 3; 1];
                                  [3; 1; 2]; [3; 2; 1]])

(* 発展1 *)

let reverse' xs =
  fold_right (fun x f -> compose f (cons x)) xs id []

(* 動作例 *)
let _ = assert (reverse' [1; 2; 3] = [3; 2; 1])

(* 発展2 *)

let fold_right' f xs init  =
  fold_left (fun g x -> compose g (f x)) id xs init
let fold_left' f init xs  =
  let f' = flip f in
  fold_right (fun x g -> compose g (f' x)) xs id init

(* 動作例 *)
let _ = assert (fold_right' cons [1; 2; 3] [] = [1; 2; 3])
let _ = assert (fold_left' (flip cons) [] [1; 2; 3] = [3; 2; 1])
