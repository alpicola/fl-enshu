{-# Language TupleSections #-}

import Control.Applicative

import Data.Maybe
import Data.List
import qualified Data.Set as S
import qualified PQueue as Q

main = do
  print . fromJust $ astar start goal ((\\ wall) . move1) dist1
  print . fromJust $ bestFirst start goal ((\\ wall) . move1) dist1
  print . fromJust $ astar start goal ((\\ wall) . move2) dist2
  print . fromJust $ bestFirst start goal ((\\ wall) . move2) dist2
  print . fromJust $ astar start goal ((\\ wall) . move3) dist3
  print . fromJust $ bestFirst start goal ((\\ wall) . move3) dist3
 where
  start = (0, 0)
  goal = (10, 1)
  wall = map (4,) [-1..1] ++ map (,-2) [3..6] ++ map (8,) [0..4]

type Point = (Int, Int)

move1 :: Point -> [Point]
move1 (x, y) =
  [(x+1, y), (x-1, y), (x, y+1), (x, y-1)]

dist1 :: Point -> Point -> Int
dist1 (x, y) (x', y') = abs (x - x') + abs (y - y')

move2 :: Point -> [Point]
move2 (x, y) =
  [(x-1, y-1), (x-1, y), (x-1, y+1), (x, y-1),
   (x, y+1), (x+1, y-1), (x+1, y), (x+1, y+1)]

dist2 :: Point -> Point -> Int
dist2 (x, y) (x', y') = abs (x - x') `max` abs (y - y')

move3 :: Point -> [Point]
move3 (x, y) =
  [(x+1, y+2), (x-1, y+2), (x+1, y-2), (x-1, y-2),
   (x+2, y+1), (x-2, y+1), (x+2, y-1), (x-2, y-1)]

dist3 :: Point -> Point -> Int
dist3 (x, y) (x', y') = (abs (x - x') + abs (y - y')) `div` 3

astar :: (Eq a, Ord a, Ord k, Num k) =>
         a -> a -> (a -> [a]) -> (a -> a -> k) -> Maybe [a]
astar start goal succ dist =
  reverse <$> go S.empty (Q.singleton (h start) [start])
 where
  h = dist goal 
  go seen queue = do
    ((c, path@(n:_)), q) <- Q.deleteFindMin queue
    if n == goal then
      return path
    else
      let ns = filter (flip S.notMember seen) $ succ n
          c' = c - h n + 1
          cs = map ((+ c') . h) ns
      in go (S.insert n seen) $
           foldl' (flip $ uncurry Q.insert) q $ zip cs (map (:path) ns)

bestFirst :: (Eq a, Ord a, Ord k) =>
             a -> a -> (a -> [a]) -> (a -> a -> k) -> Maybe [a]
bestFirst start goal succ dist =
  reverse <$> go S.empty (Q.singleton (h start) [start])
 where
  h = dist goal
  go seen queue = do
    ((_, path@(n:_)), q) <- Q.deleteFindMin queue
    if n == goal then
      return path
    else
      let ns = filter (flip S.notMember seen) $ succ n
          cs = map h ns
      in go (S.insert n seen) $
           foldl' (flip $ uncurry Q.insert) q $ zip cs (map (:path) ns)
