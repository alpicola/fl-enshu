{-# Language BangPatterns #-}

import Control.Monad

import Data.List
import Data.Maybe

main = do
  print . fromJust $ iddfs t2
 where
  t0 = iterate (\t -> Branch t t) (Branch (Unit 1) None) !! 20
  t1 = Branch (Branch None t1)
              (Branch (Branch (Unit 10) None) None)
  t2 = genTree [] (\n -> map (:n) [1..]) ((== 180) . sum . map (\x -> x * x)) 

data SearchTree a = None | Unit a | Branch (SearchTree a) (SearchTree a)

genTree :: a -> (a -> [a]) -> (a -> Bool) -> SearchTree a
genTree start succ pred = go start
 where
  go n | pred n    = Unit n
       | otherwise = case succ n of
                       [] -> None
                       xs -> foldr1 Branch $ map go xs

dfs :: SearchTree a -> Maybe a
dfs t = go [t]
 where
  go [] = Nothing
  go (Unit x : _) = Just x
  go (None : ts) = go ts
  go (Branch l r : ts) = go (l:r:ts) 

bfs :: SearchTree a -> Maybe a
bfs t = go [t] []
 where
  go [] [] = Nothing
  go [] ts = go (reverse ts) []
  go (Unit x : _) _ = Just x
  go (None : ts) ts' = go ts ts'
  go (Branch l r : ts) ts' = go ts (r:l:ts')

iddfs :: SearchTree a -> Maybe a
iddfs t = msum $ map (go [(t, 0)]) [1..]
 where
  go [] _ = Nothing
  go ((Unit x, _) : _) _ = Just x
  go ((None, _) : rest) limit = go rest limit
  go ((Branch l r, n) : rest) limit
    | n < limit = let !m = n + 1 in go ((l, m) : (r, m) : rest) limit
    | otherwise = go rest limit
