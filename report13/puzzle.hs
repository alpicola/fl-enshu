import Control.Applicative ((<$>))
import Control.Monad
import Control.Monad.State.Strict

import Data.Maybe
import Data.List
import qualified Data.Set as S
import Data.Vector (Vector)
import qualified Data.Vector as V
import qualified PQueue as Q

boardSize :: Int
boardSize = 4

main = print . map V.toList . fromJust $ idastar start goal move dist
 where
  start = V.fromList [1, 4, 14, 13, 11, 9, 8, 12,
                      3, 6, 0, 7, 15, 5, 2, 10]
  -- start = V.fromList [1, 3, 2, 4, 11, 6, 7, 8,
  --                     9, 10, 5, 12, 13, 14, 15, 0]
  -- start = V.fromList [8, 6, 7, 2, 5, 4, 3, 0, 1]
  goal = V.fromList ([1 .. boardSize * boardSize - 1] ++ [0])

type Board = Vector Int

neighbors :: Vector [Int]
neighbors = V.generate (boardSize * boardSize) $ \i -> do
  let (x, y) = i `divMod` boardSize
  (x', y') <- [(x+1, y), (x-1, y), (x, y+1), (x, y-1)]
  guard $ 0 <= x' && x' < boardSize
  guard $ 0 <= y' && y' < boardSize
  return $ x' * boardSize + y'

move :: Board -> [Board]
move ns = do
  let i = fromJust $ V.elemIndex 0 ns
  j <- neighbors V.! i
  return $ ns V.// [(i, ns V.! j), (j, 0)]

dist :: Board -> Board -> Int
dist goal = V.sum . V.imap (\i n -> table V.! (n * s + i))
 where
  s = boardSize * boardSize
  table = V.generate (s * s) $ \k ->
            let (n, i) = k `divMod` s
                j = fromJust $ V.elemIndex n goal
                (x, y) = i `divMod` boardSize
                (z, w) = j `divMod` boardSize
            in if n == 0 then 0
                         else abs (x - z) + abs (y - w)

astar :: (Eq a, Ord a) =>
         a -> a -> (a -> [a]) -> (a -> a -> Int) -> Maybe [a]
astar start goal succ dist =
  reverse <$> go S.empty (Q.singleton (h start) [start])
 where
  h = dist goal 
  go seen queue = do
    ((c, path@(n:_)), q) <- Q.deleteFindMin queue
    if n == goal then
      return path
    else
      let ns = filter (flip S.notMember seen) $ succ n
          c' = length path
          cs = map ((+ c') . h) ns
      in go (S.insert n seen) $
           foldl' (flip $ uncurry Q.insert) q $ zip cs (map (:path) ns)

idastar :: (Eq a, Ord a) =>
           a -> a -> (a -> [a]) -> (a -> a -> Int) -> Maybe [a]
idastar start goal succ dist = idastar' (h start)
 where
  h = dist goal
  idastar' limit =
    case runState (dfs (h start) [start]) maxBound of
      (Just path, _) -> Just $ reverse path
      (_, c) | c == maxBound -> Nothing
             | otherwise     -> idastar' c
   where
    dfs c path@(n:ns)
      | n == goal = return $ Just path
      | c > limit = modify' (min c) >> return Nothing
      | otherwise =
        let c' = length path
            f n' cont = do
              result <- dfs (c' + h n') (n':path)
              case result of
                Nothing -> cont
                _ -> return result
        in foldr f (return Nothing) (succ n \\ ns)
    modify' f = get >>= (\x -> put $! f x)
