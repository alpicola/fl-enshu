module PQueue (
    PQueue(), null, singleton, union, insert, deleteFindMin, fromList
  ) where

import Prelude hiding (null)
import Data.List (foldl')

data PQueue k a = Empty | PQueue k a !(PQueue k a) (PQueue k a) 

null :: Ord k => PQueue k a -> Bool
null Empty = True
null _ = False

singleton :: Ord k => k -> a -> PQueue k a
singleton k a = PQueue k a Empty Empty

union :: Ord k => PQueue k a -> PQueue k a -> PQueue k a
union Empty q = q
union q Empty = q
union q@(PQueue k a l r) q'@(PQueue k' a' l' r')
  | k <= k'   = case l of
                  Empty -> PQueue k a q' r
                  _ -> PQueue k a Empty (union (union q' l) r)
  | otherwise = case l' of
                  Empty -> PQueue k' a' q r'
                  _ -> PQueue k' a' Empty (union (union q l') r')

insert :: Ord k => k -> a -> PQueue k a -> PQueue k a
insert k a q = union (singleton k a) q

deleteFindMin :: Ord k => PQueue k a -> Maybe ((k, a), PQueue k a)
deleteFindMin Empty = Nothing
deleteFindMin (PQueue k a l r) = return ((k, a), union l r)

fromList :: Ord k => [(k, a)] -> PQueue k a
fromList = foldl' (flip $ uncurry insert) Empty
