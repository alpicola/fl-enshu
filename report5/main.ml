open Types
open Eval
open Printf

let repl () =
  try
    let rec repl' env =
      print_string "# ";
      let input = read_line () in
      try 
        let lexbuf = Lexing.from_string input in
        match Parser.main Lexer.token lexbuf with
          | [] -> repl' env
          | cmd :: _ ->
            let (mes, env) = exec env cmd in
            print_endline mes;
            repl' env
      with
        | Failure mes ->
            print_endline mes;
            repl' env
        | Parsing.Parse_error ->
            print_endline "parser: syntax error";
            repl' env in
    repl' []
  with
    | End_of_file -> ()

let _ =
  if Array.length Sys.argv > 1
    then 
      try 
        let lexbuf = Lexing.from_channel (open_in Sys.argv.(1)) in
        let cmds = Parser.main Lexer.token lexbuf in
        List.fold_left (fun env cmd ->
          try
            let (mes, env) = exec env cmd in
            print_endline mes; env
          with
            | Failure mes ->
                print_endline mes; env
            | Parsing.Parse_error ->
                print_endline "parser: syntax error"; env) [] cmds; ()
      with
        | End_of_file -> ()
    else repl ()
