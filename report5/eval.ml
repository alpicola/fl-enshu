open Types

let rec pattern_match = function
  | ConstPat v, v' when v = v' -> Some [] 
  | ConsPat (pat, pat'), ListVal (v :: vs) ->
      (match pattern_match (pat, v) with
        | None -> None
        | Some binding ->
            (match pattern_match (pat', ListVal vs) with
              | None -> None
              | Some binding' -> Some (binding @ binding')))
  | TuplePat [], TupleVal [] -> Some []
  | TuplePat (pat :: pats), TupleVal (v :: vs) ->
      (match pattern_match (pat, v) with
        | None -> None
        | Some binding ->
            (match pattern_match (TuplePat pats, TupleVal vs) with
              | None -> None
              | Some binding' -> Some (binding @ binding')))
  | PatVar var, v -> Some [(var, v)] 
  | _ -> None

let rec eval env = function
  | Const v -> v
  | Var var ->
      (try
         List.assoc var env
       with
         | Not_found -> raise (Eval_error ("unbound variable: " ^ var)))
  | Tuple es -> TupleVal (List.map (eval env) es)
  | Nil -> ListVal []
  | Cons (e, e') ->
      (match eval env e' with
         | ListVal vals -> ListVal (eval env e :: vals) 
         | _ -> raise (Eval_error "required list"))
  | Add (e, e') ->
      (match (eval env e, eval env e') with
         | (IntVal i, IntVal i') -> IntVal (i + i')
         | _ -> raise (Eval_error "required int"))
  | Sub (e, e') ->
      (match (eval env e, eval env e') with
         | (IntVal i, IntVal i') -> IntVal (i - i')
         | _ -> raise (Eval_error "required int"))
  | Mul (e, e') ->
      (match (eval env e, eval env e') with
         | (IntVal i, IntVal i') -> IntVal (i * i')
         | _ -> raise (Eval_error "required int"))
  | Div (e, e') ->
      (match (eval env e, eval env e') with
         | (IntVal i, IntVal i') -> IntVal (i / i')
         | _ -> raise (Eval_error "required int"))
  | Lt (e, e') ->
      (match (eval env e, eval env e') with
         | (IntVal i, IntVal i') -> BoolVal (i < i')
         | _ -> raise (Eval_error "required int"))
  | Eq  (e, e') ->
      (match (eval env e, eval env e') with
         | (IntVal i, IntVal i') -> BoolVal (i = i')
         | _ -> raise (Eval_error "required int"))
  | If (e, e', e'') ->
      (match eval env e with
         | BoolVal true -> eval env e'
         | BoolVal false -> eval env e''
         | _ -> raise (Eval_error "required bool"))
  | Let (var, e, e') ->
      eval ((var, eval env e) :: env) e'
  | LetRec (recs, e) ->
      let rec env' = lazy (List.map (fun (var, var', e') ->
        (var, RecFunVal (var', e', env'))) recs @ env) in
      eval (Lazy.force env') e
  | Fun (var, e) -> FunVal (var, e, env)
  | App (e, e') ->
      (match eval env e with
         | FunVal (var, body, env')
         | RecFunVal (var, body, lazy env') ->
             eval ((var, eval env e') :: env') body
         | _ -> raise (Eval_error "required fun"))
  | Match (e, clauses) ->
      let v = eval env e in
      let rec iter = function
        | [] -> raise (Eval_error "pattern match failed")
        | ((pattern, e') :: clauses) ->
            (match pattern_match (pattern, v) with
              | None -> iter clauses 
              | Some binding -> eval (binding @ env) e') in
      iter clauses

let exec env = function
  | Expr e -> 
      (try
         let result = eval env e in
         (string_of_val result, env)
       with 
         | Eval_error mes -> ("eval: " ^ mes, env))
  | Def (var, e) ->
      (try
         let result = eval env e in
         (var ^ " = " ^ string_of_val result, (var, result) :: env)
       with 
         | Eval_error mes -> ("eval: " ^ mes, env))
  | DefRec recs ->
      let rec env' = lazy (List.map (fun (var, var', e) ->
        (var, RecFunVal (var', e, env'))) recs @ env) in
      let mes = String.concat "\n" (List.map (fun (var, var', _) ->
        var ^ " = fun " ^ var' ^ " -> ...") recs) in
      (mes, Lazy.force env')
