(* recursion *)

let rec fact n = if n = 1 then 1 else n * (fact (n - 1));; 

fact 10;;

let rec map f xs = match xs with [] -> []
                               | x :: xs -> f x :: map f xs;;

map (fun x -> x + 1) [1; 2; 3];;

(* mutual recursion *)

let rec even n = if n = 0 then true else not (odd (n - 1))
    and odd n  = if n = 0 then false else not (even (n - 1));;

even 10;;
