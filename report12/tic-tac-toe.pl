player(o).
player(x).

opponent(P, Q) :- player(P), player(Q), P \= Q.

board0([[b, b, b],
        [b, b, b],
        [b, b, b]]).
board1([[o, b, b],
        [x, b, b],
        [b, b, b]]).
board2([[b, b, b],
        [x, o, b],
        [b, b, b]]).
board3([[b, o, b],
        [x, b, b],
        [b, b, b]]).

line(P, [[P, P, P], _, _]).
line(P, [_, [P, P, P], _]).
line(P, [_, _, [P, P, P]]).
line(P, [[P, _, _],
         [P, _, _],
         [P, _, _]]).
line(P, [[_, P, _],
         [_, P, _],
         [_, P, _]]).
line(P, [[_, _, P],
         [_, _, P],
         [_, _, P]]).
line(P, [[P, _, _],
         [_, P, _],
         [_, _, P]]).
line(P, [[_, _, P],
         [_, P, _],
         [P, _, _]]).

mark(P, [X0, Y, Z], [X1, Y, Z]) :- mark_row(P, X0, X1).
mark(P, [X, Y0, Z], [X, Y1, Z]) :- mark_row(P, Y0, Y1).
mark(P, [X, Y, Z0], [X, Y, Z1]) :- mark_row(P, Z0, Z1).
mark_row(P, [b, X, Y], [P, X, Y]).
mark_row(P, [X, b, Y], [X, P, Y]).
mark_row(P, [X, Y, b], [X, Y, P]).

win(P, B) :- line(P, B).
win(P, B) :- opponent(P, Q), mark(P, B, C), lose(Q, C).
lose(P, B) :- opponent(P, Q), line(Q, B).
lose(P, B) :- mark(P, B, _), \+ mark_and_not_lose(P, B).
tie(P, B) :- \+ win(P, B), \+ lose(P, B).
mark_and_not_lose(P, B) :-
    opponent(P, Q), mark(P, B, C), not_win(Q, C).
not_win(P, B) :- lose(P, B).
not_win(P, B) :- \+ win(P, B).
