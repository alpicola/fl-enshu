data Vertex = A | B | C | D | E
type Edge = (Vertex, Vertex)
type Graph = ([Vertex], [Edge])
type Path = [Vertex]

g1 :: Graph
g1 = ([A,B,C,D,E], [(A,D),(B,A),(C,A),(C,B),(D,C),(E,A),(E,D)])

one :: [a] -> a
one = foldr1 (?)

delete :: a -> [a] -> [a]
delete x [] = []
delete x (y:ys) = if x == y then ys
                            else y : delete x ys

simplePath :: Graph -> Path
simplePath (v@(_:_), _) = one [[v0] | v0 <- v]
simplePath (v@(_:_), e@(_:_)) | head p =:= v2 = v1 : p 
 where
  (v1, v2) = one e
  v' = delete v1 v
  e' = filter (\(v3, v4) -> v3 /= v1 && v4 /= v1) e
  p = simplePath (v', e')

hamiltonPath :: Graph -> Path
hamiltonPath (v, e) | length p =:= length v = p
 where
  p = simplePath (v, e)
