take :: Int -> [a] -> [a]
take _ [] = []
take n (x:xs) = if n > 0 then x : take (n-1) xs
                         else []

drop :: Int -> [a] -> [a]
drop _ [] = []
drop n (x:xs) = if n > 0 then drop (n-1) xs
                         else x:xs

head :: [a] -> a
head (x:_) = x

tail :: [a] -> [a]
tail (_:xs) = xs

last :: [a] -> a
last (x:xs) = last' x xs
 where
  last' x [] = x
  last' _ (x:xs) = last' x xs

init :: [a] -> [a]
init (x:xs) = init' x xs
 where
  init' _ [] = []
  init' x (y:ys) = x : init' y ys

sillyUnparen :: String -> String
sillyUnparen [] = []
sillyUnparen [c] = [c]
sillyUnparen s@(_:_:_)
  | head s == '(' && last s == ')' = tail (init s)
  | otherwise                      = s

data Paren = PSeq Paren Paren | PEmp

paren :: Paren -> String
paren PEmp = ""
paren (PSeq p1 p2) = "(" ++ paren p1 ++ ")" ++ paren p2

unparen :: String -> Paren
unparen "" = PEmp
unparen s | "(" ++ paren p1 ++ ")" ++ paren p2 =:= s = PSeq p1 p2
 where p1, p2 free

