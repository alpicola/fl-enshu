data Family = A | B | C | D | E | F | G | H | I | J

parent :: Family -> Family
parent A = D
parent B = D
parent C = D
parent A = E
parent B = E
parent D = G
parent F = G
parent D = H
parent F = H
parent I = F
parent J = F

sibling :: Family -> Family
sibling x | parent x =:= parent y & x =/= y = y
 where y free

ancestor :: Family -> Family
ancestor x = parent x
ancestor x | parent x =:= y = ancestor y
 where y free

bloodrelative :: Family -> Family
bloodrelative x = ancestor x
bloodrelative x | ancestor y =:= x = y
 where y free
bloodrelative x | ancestor x =:= ancestor y & x =/= y = y
 where y free
