-- 問1

insSort :: Ord a => [a] -> [a]
insSort xs = foldl (flip ins) [] xs
 where
  ins x [] = [x]
  ins x (y:ys)
    | x < y     = x : y : ys 
    | otherwise = y : ins x ys

quickSort :: Ord a => [a] -> [a]
quickSort [] = []
quickSort (x:xs) =
  let (lesser, greater) = partition (< x) xs
  in quickSort lesser ++ [x] ++ quickSort greater
 where
  partition p xs = partition' xs ([], [])
   where
    partition' [] r = r 
    partition' (x:xs) (ys, zs)
      | p x       = partition' xs (x:ys, zs)
      | otherwise = partition' xs (ys, x:zs)

selectionSort :: Ord a => [a] -> [a]
selectionSort [] = []
selectionSort xs = let m = minimum xs in m : selectionSort (del m xs)
 where
  del x [] = []
  del x (y:ys)
    | x == y    = ys
    | otherwise = y : del x ys

mergeSort :: Ord a => [a] -> [a]
mergeSort [] = []
mergeSort [x] = [x]
mergeSort xs = let (ys, zs) = split xs in merge (mergeSort ys) (mergeSort zs)
 where
  split xs = split' xs ([], [])
   where
    split' [] r = r
    split' [x] (ys, zs) = (x:ys, zs)
    split' (x:x':xs) (ys, zs) = split' xs (x:ys, x':zs)
  merge xs [] = xs
  merge [] ys = ys
  merge (x:xs) (y:ys)
    | x < y     = x : merge xs (y:ys)
    | otherwise = y : merge (x:xs) ys

-- 問2

ones :: [Integer]
ones = 1 : ones

from :: Integer -> [Integer]
from n = n : from (n + 1)

nats :: [Integer]
nats = from 1

fibs :: [Integer]
fibs = 1 : 1 : zipWith (+) fibs (tail fibs)

primes :: [Integer]
primes = 2 : filter (\n -> all ((/= 0) . mod n) $ takeWhile ((<= n) . sq) primes) (from 3)
 where
  sq x = x * x

-- 問3

infiniteTreeSearch :: (a -> [a]) -> a -> [a]
infiniteTreeSearch f x = x : go [f x]
 where
  go xss = let (xs', xss') = unzip $ map (\(x:xs) -> (x, xs)) xss
           in xs' ++ go (map f xs' ++ xss')

finiteSublists :: [a] -> [[a]]
finiteSublists xs = infiniteTreeSearch (\l -> map (:l) xs) [] 

natLists = finiteSublists nats

-- 問4

class ListLike f where
 nil :: f a
 cons :: a -> f a -> f a
 app :: f a -> f a -> f a
 toList :: f a -> [a]
 fromList :: [a] -> f a

newtype C a = C ([a] -> [a])

instance ListLike C where
 nil = C id
 cons x (C f) = C $ f . (x:)
 app (C f) (C g) = C $ g . f
 toList (C f) = f []
 fromList = foldl (flip cons) nil

reverse' :: [a] -> [a]
reverse' = toList . foldr cons (nil :: C a)
