reverse(Xs, Ys) :- reverse_append(Xs, [], Ys).
reverse_append([], Xs, Xs).
reverse_append([X | Xs], Zs, Ys) :- reverse_append(Xs, [X | Zs], Ys).

concat([Xs], Xs).
concat([Xs | Xss], Ys) :- append(Xs, Zs, Ys), concat(Xss, Zs).
