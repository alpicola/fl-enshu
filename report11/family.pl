parent(a, d).
parent(b, d).
parent(c, d).
parent(a, e).
parent(b, e).
parent(d, g).
parent(f, g).
parent(d, h).
parent(f, h).
parent(i, f).
parent(j, f).

id(X, X).

sibling(X, Y) :- parent(X, Z), parent(Y, Z), \+ id(X, Y).
ancestor(X, Y) :- parent(X, Y).
ancestor(X, Y) :- parent(X, Z), ancestor(Z, Y). 
bloodrelative(X, Y) :- ancestor(X, Y).
bloodrelative(X, Y) :- ancestor(Y, X).
bloodrelative(X, Y) :- ancestor(X, Z), ancestor(Y, Z), \+ id(X, Y).
