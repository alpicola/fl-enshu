contains(X, [X | _]).
contains(X, [_ | Ys]) :- contains(X, Ys).

subset([], _).
subset([X | Xs], Ys) :- contains(X, Ys), subset(Xs, Ys).

delete(X, [X | Ys], Ys).
delete(X, [Y | Ys], [Y | Zs]) :- delete(X, Ys, Zs).

simple_path([X, Y], V, E) :-
    contains((X, Y), E), delete(X, V, W), contains(Y, W).
simple_path([X, Y | Xs], V, E) :-
    contains((X, Y), E), delete(X, V, W), simple_path([Y | Xs], W, E).

hamilton_path(P, V, E) :- simple_path(P, V, E), subset(V, P).
hamilton(V, E) :- hamilton_path(_, V, E).
