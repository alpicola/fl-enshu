(*
 * 以下のようにして動作確認
 *   % ocaml report.ml
 *)

(* 問1 *)

type 'a tree = Leaf
             | Node of 'a * 'a tree * 'a tree

let rec dfs_preorder = function
  | Leaf -> []
  | Node (a, t1, t2) -> a :: (dfs_preorder t1 @ dfs_preorder t2)

let rec dfs_inorder = function
  | Leaf -> []
  | Node (a, t1, t2) -> dfs_inorder t1 @ (a :: dfs_inorder t2)

let rec dfs_postorder = function
  | Leaf -> []
  | Node (a, t1, t2) -> dfs_postorder t1 @ dfs_postorder t2 @ [a]

(* 動作例 *)
let t = Node (1,
              Node (2,
                    Leaf,
                    Node (3, Leaf, Leaf)),
              Node (4,
                    Node (5, Leaf, Leaf),
                    Leaf))
let _ = assert (dfs_preorder t = [1; 2; 3; 4; 5])
let _ = assert (dfs_inorder t = [2; 3; 1; 5; 4])
let _ = assert (dfs_postorder t = [3; 2; 5; 4; 1])

(* 問2 *)

let rec bfs t =
  let rec bfs' xs = function
    | [] -> xs
    | ts -> let (ys, ts) = List.fold_right (fun t (ys, ts) -> match t with
              | Leaf -> (ys, ts)
              | Node (y, t1, t2) -> (y :: ys, t1 :: t2 :: ts)) ts ([], []) in
            bfs' (xs @ ys) ts in
  bfs' [] [t]

(* 動作例 *)
let _ = assert (bfs t = [1; 2; 4; 3; 5])

(* 問3 *)

let fix1 f =
  let r = ref (fun f x -> failwith "") in
  let fix f x = f (!r f) x in
  r := fix; fix f

(* 動作例 *)
let fact1 =
  fix1 (fun f n ->
         if n = 1 then 1 else n * f (n - 1))
let _ = assert (fact1 10 = 3628800)

(* 問4 *)

type 'a rec_t = In of ('a rec_t -> 'a) 
let out (In x) = x

let fix2 f = (fun x y -> f (out x x) y)
             (In (fun x y -> f (out x x) y))

(* 動作例 *)
let fact2 =
  fix2 (fun f n ->
         if n = 1 then 1 else n * f (n - 1))
let _ = assert (fact2 10 = 3628800)

(* 問5 *)

type value = VInt of int 
           | VBool of bool

type expr = EConst of value
          | EAdd of expr * expr
          | ESub of expr * expr
          | EMul of expr * expr
          | EDiv of expr * expr
          | EEq of expr * expr
          | ELt of expr * expr
          | EIf of expr * expr * expr

(* 問6 *)

exception Eval_error

let rec eval = function
  | EConst v -> v
  | EAdd (e, e') ->
      (match (eval e, eval e') with
         | (VInt i, VInt i') -> VInt (i + i')
         | _ -> raise Eval_error)
  | ESub (e, e') ->
      (match (eval e, eval e') with
         | (VInt i, VInt i') -> VInt (i - i')
         | _ -> raise Eval_error)
  | EMul (e, e') ->
      (match (eval e, eval e') with
         | (VInt i, VInt i') -> VInt (i * i')
         | _ -> raise Eval_error)
  | EDiv (e, e') ->
      (match (eval e, eval e') with
         | (VInt i, VInt i') -> VInt (i / i')
         | _ -> raise Eval_error)
  | EEq  (e, e') ->
      (match (eval e, eval e') with
         | (VInt i, VInt i') -> VBool (i = i')
         | (VBool b, VBool b') -> VBool (b = b')
         | _ -> raise Eval_error)
  | ELt (e, e') ->
      (match (eval e, eval e') with
         | (VInt i, VInt i') -> VBool (i < i')
         | _ -> raise Eval_error)
  | EIf (e, e', e'') ->
      (match eval e with
         | VBool true -> eval e'
         | VBool false -> eval e''
         | _ -> raise Eval_error)

(* 動作例 *)
let e = EIf (ELt (EConst (VInt 1), EConst (VInt 2)),
             EAdd (EConst (VInt 3), EMul (EConst (VInt 4), EConst (VInt 5))),
             EConst (VInt 0))
let _ = assert (eval e = VInt 23)
