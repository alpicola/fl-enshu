{-# Language GADTs, RankNTypes #-}

import Control.Applicative
import Control.Monad.Identity
import Control.Monad.Reader

import Data.Char
import Data.List
import Data.Tuple

main = do
  putStrLn $ ppr expr1
  print $ eval expr1
  print $ eval expr2
  print $ eval expr3
 where
  expr1 = Let (Abs $ \n -> (Add (Int 1) (Var n))) $ \succ ->
            Let (Int 1) $ \x ->
              Let (Int 2) $ \y ->
                Add (App (Var succ) (Var x)) (App (Var succ) (Var y))
  expr2 = fact (Int 10)
  -- expr3 = iterate (\x -> Add x x) (Int 3) !! 30
  expr3 = iterate (App (Abs $ \x -> Add (Var x) (Var x))) (Int 3) !! 30

data ExprP p a where
  Int :: Int -> ExprP p Int
  Bool :: Bool -> ExprP p Bool
  Add :: ExprP p Int -> ExprP p Int -> ExprP p Int
  Sub :: ExprP p Int -> ExprP p Int -> ExprP p Int
  Mul :: ExprP p Int -> ExprP p Int -> ExprP p Int
  Div :: ExprP p Int -> ExprP p Int -> ExprP p Int
  Lt :: ExprP p Int -> ExprP p Int -> ExprP p Bool
  Eq :: Eq a => ExprP p a -> ExprP p a -> ExprP p Bool
  If :: ExprP p Bool -> ExprP p a -> ExprP p a -> ExprP p a
  Abs :: (p a -> ExprP p b) -> ExprP p (a -> b)
  App :: ExprP p (a -> b) -> ExprP p a -> ExprP p b
  Var :: p a -> ExprP p a
  Let :: ExprP p a -> (p a -> ExprP p b) -> ExprP p b
 
type Expr a = forall p. ExprP p a

eval :: ExprP Identity a -> a
eval (Int i) = i
eval (Bool b) = b
eval (Add e e') = eval e + eval e'
eval (Sub e e') = eval e - eval e'
eval (Mul e e') = eval e * eval e'
eval (Div e e') = eval e `div` eval e'
eval (Lt e e') = eval e < eval e'
eval (Eq e e') = eval e == eval e'
eval (If e e' e'') = if eval e then eval e' else eval e''
eval (Abs f) = eval . f . Identity
eval (App e e') = eval e (eval e')
eval (Var var) = runIdentity var
eval (Let e f) = eval (Abs f) (eval e)
 
fact :: Expr Int ->  Expr Int
fact e = If (Eq e (Int 0))
            (Int 1)
            (Mul e (fact (Sub e (Int 1))))

newtype Place a = Place Int

instance Show (Place a) where
 show (Place 0) = "a"
 show (Place i) =
   reverse . map (chr . (+ 97)) $ flip unfoldr i $ \n ->
     if n == 0 then Nothing else return $ swap $ divMod n 26

ppr :: ExprP Place a -> String
ppr = flip runReader 0 . ppr' False
 where
  ppr' :: Bool -> ExprP Place a -> Reader Int String
  ppr' _ (Int i) = return $ show i
  ppr' _ (Bool b) = return $ show b
  ppr' p (Add e e') = do
    s <- ppr' True e; s' <- ppr' True e'
    return . paren p $ s ++ " + " ++ s'
  ppr' p (Sub e e') = do
    s <- ppr' True e; s' <- ppr' True e'
    return . paren p $ s ++ " - " ++ s'
  ppr' p (Mul e e') = do
    s <- ppr' True e; s' <- ppr' True e'
    return . paren p $ s ++ " * " ++ s'
  ppr' p (Div e e') = do
    s <- ppr' True e; s' <- ppr' True e'
    return . paren p $ s ++ " / " ++ s'
  ppr' p (Lt e e') = do
    s <- ppr' True e; s' <- ppr' True e'
    return . paren p $ s ++ " < " ++ s'
  ppr' p (Eq e e') = do
    s <- ppr' True e; s' <- ppr' True e'
    return . paren p $ s ++ " = " ++ s'
  ppr' p (If e e' e'') = do
    s <- ppr' False e; s' <- ppr' False e'; s'' <- ppr' False e''
    return . paren p $ "if " ++ s ++ " then " ++ s' ++ " else " ++ s''
  ppr' p (Abs f) = do
    var <- asks Place
    body <- local (+1) $ ppr' False (f var)
    return . paren p $ "\\" ++ show var ++ " -> " ++ body
  ppr' _ (App e e') = do
    s <- ppr' True e; s' <- ppr' True e'
    return $ s ++ " " ++ s'
  ppr' _ (Var var) = return $ show var
  ppr' p (Let e f) = do
    s <- ppr' False e
    var <- asks Place
    body <- local (+1) $ ppr' False (f var)
    return . paren p $ "let " ++ show var ++ " = " ++ s ++ " in " ++ body

  paren :: Bool -> String -> String
  paren p s = if p then "(" ++ s ++ ")" else s
