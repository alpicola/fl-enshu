type value = IntVal of int 
           | BoolVal of bool
           | FunVal of string * env * expr
           | TupleVal of (obj ref) list
           | ConsVal of obj ref * obj ref
           | NilVal
and obj = Value of value
        | Thunk of env * expr
and expr = Const of value
         | Var of string
         | Tuple of expr list
         | Cons of expr * expr
         | Nil
         | Add of expr * expr
         | Sub of expr * expr
         | Mul of expr * expr
         | Div of expr * expr
         | Lt of expr * expr
         | Eq of expr * expr
         | If of expr * expr * expr
         | Let of string * expr * expr
         | LetRec of (string * expr) list * expr
         | Fun of string * expr
         | App of expr * expr
         | Match of expr * (pattern * expr) list
         | Undefined
and env = (string * obj ref) list
and pattern = ConstPat of value
            | ConsPat of pattern * pattern
            | NilPat
            | TuplePat of pattern list
            | PatVar of string

type cmd = Expr of expr
         | Def of string * expr
         | DefRec of (string * expr) list

exception Eval_error of string
