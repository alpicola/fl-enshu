open Types

let thunk env expr = ref (Thunk (env, expr))

let rec eval env = function
  | Const v -> v
  | Var var ->
      (try
        eval' (List.assoc var env)
       with
         | Not_found -> raise (Eval_error ("unbound variable: " ^ var)))
  | Tuple es -> TupleVal (List.map (thunk env) es)
  | Nil -> NilVal
  | Cons (e, e') -> ConsVal (thunk env e, thunk env e')
  | Add (e, e') ->
      (match (eval env e, eval env e') with
         | (IntVal i, IntVal i') -> IntVal (i + i')
         | _ -> raise (Eval_error "required int"))
  | Sub (e, e') ->
      (match (eval env e, eval env e') with
         | (IntVal i, IntVal i') -> IntVal (i - i')
         | _ -> raise (Eval_error "required int"))
  | Mul (e, e') ->
      (match (eval env e, eval env e') with
         | (IntVal i, IntVal i') -> IntVal (i * i')
         | _ -> raise (Eval_error "required int"))
  | Div (e, e') ->
      (match (eval env e, eval env e') with
         | (IntVal i, IntVal i') -> IntVal (i / i')
         | _ -> raise (Eval_error "required int"))
  | Lt (e, e') ->
      (match (eval env e, eval env e') with
         | (IntVal i, IntVal i') -> BoolVal (i < i')
         | _ -> raise (Eval_error "required int"))
  | Eq  (e, e') ->
      (match (eval env e, eval env e') with
         | (IntVal i, IntVal i') -> BoolVal (i = i')
         | _ -> raise (Eval_error "required int"))
  | If (e, e', e'') ->
      (match eval env e with
         | BoolVal true -> eval env e'
         | BoolVal false -> eval env e''
         | _ -> raise (Eval_error "required bool"))
  | Let (var, e, e') ->
      eval ((var, thunk env e) :: env) e'
  | LetRec (recs, e) ->
      let vars = List.map fst recs in
      let refs = List.map (fun _ -> thunk [] Undefined) vars in
      let env' = List.combine vars refs @ env in
      List.iter2 (fun (_, e') r -> r := Thunk (env', e')) recs refs;
      eval env' e
  | Fun (var, e) -> FunVal (var, env, e)
  | App (e, e') ->
      (match eval env e with
         | FunVal (var, env', body) ->
             eval ((var, thunk env e') :: env') body
         | _ -> raise (Eval_error "required fun"))
  | Match (e, clauses) ->
      let r = thunk env e in
      let rec iter = function
        | [] -> raise (Eval_error "pattern match failed")
        | ((pattern, e') :: clauses) ->
            (match pattern_match (pattern, r) with
              | None -> iter clauses 
              | Some binding -> eval (binding @ env) e') in
      iter clauses
and eval' r = match !r with
  | Value v -> v
  | Thunk (env, expr) ->
      let v = eval env expr in
      r := Value v; v
and pattern_match = function
  | PatVar var, r -> Some [(var, r)]
  | pat, r ->
      (match (pat, (eval' r)) with
        | ConstPat v, v' when v = v' -> Some [] 
        | ConsPat (pat, pat'), ConsVal (r, r') ->
            (match pattern_match (pat, r) with
              | None -> None
              | Some binding ->
                  (match pattern_match (pat', r') with
                    | None -> None
                    | Some binding' -> Some (binding @ binding')))
        | NilPat, NilVal -> Some []
        | TuplePat pats, TupleVal refs ->
            let rec iter pats refs binding = match (pats, refs) with
              | [], [] -> Some binding
              | (pat :: pats), (r :: refs) ->
                  (match pattern_match (pat, r) with
                    | None -> None
                    | Some binding' -> iter pats refs (binding @ binding')) in
            iter pats refs []
        | _ -> None)

let rec vals_of_listval = function
  | ConsVal (r, r') ->
      eval' r :: vals_of_listval (eval' r') 
  | NilVal -> []
  | _ -> raise (Eval_error "required list")

let rec string_of_val = function
  | IntVal i -> string_of_int i
  | BoolVal b -> string_of_bool b
  | FunVal (v, _, _) -> "fun " ^ v ^ " -> ..."
  | TupleVal refs ->
      "(" ^ String.concat ", " (List.map string_of_objref refs) ^ ")"
  | (ConsVal _ | NilVal) as v ->
      let vals = vals_of_listval v in
      "[" ^ String.concat "; " (List.map string_of_val vals) ^ "]"
and string_of_objref r = match !r with 
  | Value v -> string_of_val v
  | Thunk (env, expr) -> string_of_val (eval env expr)

let exec env = function
  | Expr e -> 
      (try
         let result = eval env e in
         (string_of_val result, env)
       with 
         | Eval_error mes -> ("eval: " ^ mes, env))
  | Def (var, e) -> (var, (var, thunk env e) :: env)
  | DefRec recs ->
      let vars = List.map fst recs in
      let refs = List.map (fun _ -> thunk [] Undefined) vars in
      let env' = List.combine vars refs @ env in
      List.iter2 (fun (_, e') r -> r := Thunk (env', e')) recs refs;
      (String.concat "\n" vars, env')
