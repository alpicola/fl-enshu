let tl l = match l with _ :: xs -> xs;;
let rec map2 f xs ys = match (xs, ys) with
  | ([], []) -> []
  | (x :: xs, y :: ys) -> f x y :: map2 f xs ys;;
let rec take n xs =
  if n = 0
    then []
    else match xs with
      | [] -> []
      | x :: xs -> x :: take (n - 1) xs;;

let rec ones = 1 :: ones;;
let rec from n = n :: from (n + 1);;
let nats = from 1;;
let rec fibs = 1 :: 1 :: map2 (fun x y -> x + y) fibs (tl fibs);;

take 10 ones;;
take 10 nats;;
take 10 fibs;;
