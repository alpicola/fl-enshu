%{
open Types
%}

%token <int> INT
%token <string> VAR

%token LPAR RPAR
%token LBRA RBRA
%token VBAR
%token PLUS MINUS TIMES DIV
%token LT EQ
%token DAND DOR NOT
%token IF THEN ELSE
%token LET REC AND IN
%token FUN ARROW
%token MATCH WITH
%token TRUE FALSE
%token COMMA
%token COL SEMICOL
%token DCOL DSEMICOL
%token EOF

%nonassoc ELSE IN ARROW
%left AND VBAR
%left LT EQ
%right DCOL
%left PLUS MINUS
%left TIMES DIV
%left DAND DOR
%nonassoc NOT
%left VAR INT TRUE FALSE LBRA LPAR

%start main
%type <Types.cmd list> main

%%

main:
    EOF { [] }
  | cmd main { $1 :: $2 }

cmd:
    expr DSEMICOL { Expr $1 }
  | LET VAR params EQ  expr DSEMICOL
    { Def ($2, List.fold_right (fun v e -> Fun (v, e)) $3 $5) }
  | LET REC mutual_recs DSEMICOL
    { DefRec $3 }

expr:
    arg { $1 }
  | expr arg { App ($1, $2) }
  | expr PLUS expr  { Add ($1, $3) }
  | expr MINUS expr { Sub ($1, $3) }
  | expr TIMES expr { Mul ($1, $3) }
  | expr DIV expr   { Div ($1, $3) }
  | expr LT expr { Lt ($1, $3) }
  | expr EQ expr { Eq ($1, $3) }
  | expr DCOL expr { Cons ($1, $3) }
  | expr DAND expr { If ($1, $3, Const (BoolVal false)) }
  | expr DOR expr  { If ($1, Const (BoolVal true), $3) }
  | NOT expr { If ($2, Const (BoolVal false), Const (BoolVal true))} 
  | IF expr THEN expr ELSE expr { If ($2, $4, $6) }
  | LET VAR params EQ  expr IN expr
    { Let ($2, List.fold_right (fun v e -> Fun (v, e)) $3 $5, $7) }
  | LET REC mutual_recs IN expr
    { LetRec ($3, $5) }
  | FUN params1 ARROW expr
    { List.fold_right (fun v e -> Fun (v, e)) $2 $4 }
  | MATCH expr WITH match_clauses
    { Match ($2, $4) }
  | MATCH expr WITH VBAR match_clauses
    { Match ($2, $5) }

arg:
    INT   { Const (IntVal $1) }
  | TRUE  { Const (BoolVal true) }
  | FALSE { Const (BoolVal false) }
  | LBRA list_elems RBRA { $2 }
  | LPAR RPAR { Tuple [] }
  | LPAR tuple_elems RPAR { Tuple $2 }
  | VAR { Var $1 }
  | LPAR expr RPAR { $2 }

params:
    { [] }
  | VAR params { $1 :: $2 }

params1:
    VAR params { $1 :: $2 }

mutual_recs:
    mutual_rec
    { [$1] }
  | mutual_rec AND mutual_recs
    { $1 :: $3 }

mutual_rec:
    VAR params EQ expr
    { ($1, List.fold_right (fun v e -> Fun (v, e)) $2 $4) }

match_clauses:
    pattern ARROW expr
    { [($1, $3)] }
  | pattern ARROW expr VBAR match_clauses
    { ($1, $3) :: $5 }

list_elems:
    { Nil }
  | expr { Cons ($1, Nil) }
  | expr SEMICOL list_elems { Cons ($1, $3) }

tuple_elems:
    expr COMMA expr { [$1; $3] }
  | expr COMMA tuple_elems { $1 :: $3 }

pattern:
    INT   { ConstPat (IntVal $1) } 
  | TRUE  { ConstPat (BoolVal true) } 
  | FALSE { ConstPat (BoolVal false) } 
  | LBRA RBRA { NilPat }
  | pattern DCOL pattern { ConsPat ($1, $3) }
  | LPAR RPAR { TuplePat [] }
  | LPAR tuple_pat_elems RPAR { TuplePat $2 }
  | VAR { PatVar $1 } 

tuple_pat_elems:
    pattern COMMA pattern { [$1; $3] }
  | pattern COMMA tuple_pat_elems { $1 :: $3 }
