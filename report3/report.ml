open Order

(* 問2 *)

(* 動作例 *)

let _ =
  let s0 = Stack.empty in
  let s1 = Stack.push 1 s0 in
  let s2 = Stack.push 2 s1 in
  let s3 = Stack.push 3 s2 in
  let (i, s4) = Stack.pop s3 in
  assert (i = 3);
  assert (Stack.size s4 = 2);

(* 問3 *)

(* 動作例 *)

module OrderedInt =
  struct
    type t = int
    let compare x y = if x < y then LT else if x > y then GT else EQ
  end

module IntSet = Set.Make (OrderedInt)

let _ =
  let s0 = IntSet.empty in
  let s1 = IntSet.add 1 s0 in
  let s2 = IntSet.add 2 s1 in
  let s3 = IntSet.add 3 s2 in
  let s4 = IntSet.add 1 s3 in
  let s5 = IntSet.remove 3 s4 in
  assert (IntSet.mem 1 s5 = true);
  assert (IntSet.mem 3 s5 = false);
  assert (IntSet.size s5 = 2)

(* 問4 *)

(* 動作例 *)

module IntMap = Map.Make (OrderedInt)

let _ =
  let t0 = IntMap.empty in
  let t1 = IntMap.add 1 'a' t0 in
  let t2 = IntMap.add 2 'b' t1 in
  let t3 = IntMap.add 3 'c' t2 in
  let t4 = IntMap.add 1 'd' t3 in
  let t5 = IntMap.remove 3 t4 in
  assert (IntMap.get 1 t5 = 'd');
  assert (IntMap.mem 3 t5 = false);
  assert (IntMap.size t5 = 2)

(* 発展1 *)

type 'a t =
  | Leaf of 'a
  | Node of ('a * 'a) t

module rec M :
  sig
    val map : ('a -> 'b) -> 'a t -> 'b t
  end =
  struct
    let map f = function
      | Leaf a -> Leaf (f a)
      | Node t -> Node (M.map (fun (a, b) -> (f a, f b)) t)
  end

(* 動作例 *)

let t1 = Node (Leaf (1, 2))
let t2 = Node (Leaf (2, 4))

let _ = assert (M.map (fun x -> x * 2) t1 = t2)
