type 'a stack = 'a list
exception Empty_stack

let empty = []
let pop = function
  | x :: xs -> (x, xs)
  | _ -> raise Empty_stack
let push x xs = x :: xs
let rec size = function
  | _ :: xs -> 1 + size xs
  | [] -> 0
