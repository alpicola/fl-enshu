type 'a stack

val empty : 'a stack
val pop : 'a stack -> ('a * 'a stack)
val push : 'a -> 'a stack -> 'a stack
val size : 'a stack -> int
