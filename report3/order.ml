type order = LT | EQ | GT

module type ORDERED_TYPE =
  sig
    type t
    val compare : t -> t -> order
  end
