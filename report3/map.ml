open Order

module type MAP =
  sig
    type 'a t
    type key
    val empty : 'a t
    val add : key -> 'a -> 'a t -> 'a t
    val get : key -> 'a t -> 'a
    val remove : key -> 'a t -> 'a t
    val mem : key -> 'a t -> bool
    val size : 'a t -> int
  end

module Make (T : ORDERED_TYPE) : (MAP with type key = T.t) =
  struct
    type key = T.t
    type 'a t = Leaf | Node of ('a t * (key * 'a) * 'a t)
    exception Empty_map

    let empty = Leaf 
    let rec add k v = function
      | Leaf -> Node (Leaf, (k, v), Leaf)
      | Node (t1, ((k', _) as p), t2) ->
          begin match T.compare k k' with
            | LT -> Node (add k v t1, p, t2)
            | EQ -> Node (t1, (k, v), t2)
            | GT -> Node (t1, p, add k v t2)
          end
    let rec get k = function
      | Leaf -> raise Not_found
      | Node (t1, (k', v), t2) ->
          begin match T.compare k k' with
            | LT -> get k t1
            | EQ -> v
            | GT -> get k t2
          end
    let rec remove_min = function
      | Leaf -> raise Empty_map
      | Node (Leaf, p, t) -> (p, t)
      | Node (t1, p, t2) -> let (p', t3) = remove_min t1 in
                            (p', Node (t3, p, t2))
    let rec remove k = function
      | Leaf -> raise Empty_map
      | Node (t1, ((k', _) as p), t2) ->
          begin match T.compare k k' with
            | LT -> Node (remove k t1, p, t2)
            | EQ -> begin match (t1, t2) with
                      | (Leaf, _) -> t2
                      | (_, Leaf) -> t1
                      | _ -> let (p, t3) = remove_min t2 in 
                             Node (t1, p, t3)
                    end
            | GT -> Node (t1, p, remove k t2)
          end
     let rec mem k = function
      | Leaf -> false
      | Node (t1, (k', v), t2) ->
          begin match T.compare k k' with
            | LT -> mem k t1
            | EQ -> true
            | GT -> mem k t2
          end
    let rec size = function
      | Leaf -> 0
      | Node (t1, _, t2) -> 1 + size t1 + size t2
  end
