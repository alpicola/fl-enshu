open Order

module type SET =
  sig
    type t
    type elem
    val empty : t
    val add : elem -> t -> t
    val remove : elem -> t -> t
    val mem : elem -> t -> bool
    val size : t -> int
  end

module Make (T : ORDERED_TYPE) : (SET with type elem = T.t) =
  struct
    type elem = T.t
    type t = Leaf | Node of (t * elem * t)
    exception Empty_set

    let empty = Leaf 
    let rec add e = function
      | Leaf -> Node (Leaf, e, Leaf)
      | Node (t1, e', t2) ->
          begin match T.compare e e' with
            | LT -> Node (add e t1, e, t2)
            | EQ -> Node (t1, e', t2)
            | GT -> Node (t1, e', add e t2)
          end
    let rec remove_min = function
      | Leaf -> raise Empty_set
      | Node (Leaf, e, t) -> (e, t)
      | Node (t1, e, t2) -> let (e', t3) = remove_min t1 in
                            (e', Node (t3, e, t2))
    let rec remove e = function
      | Leaf -> raise Empty_set
      | Node (t1, e', t2) ->
          begin match T.compare e e' with
            | LT -> Node (remove e t1, e, t2)
            | EQ -> begin match (t1, t2) with
                      | (Leaf, _) -> t2
                      | (_, Leaf) -> t1
                      | _ -> let (e, t3) = remove_min t2 in 
                             Node (t1, e, t3)
                    end
            | GT -> Node (t1, e', remove e t2)
          end
     let rec mem e = function
      | Leaf -> false
      | Node (t1, e', t2) ->
          begin match T.compare e e' with
            | LT -> mem e t1
            | EQ -> true
            | GT -> mem e t2
          end
    let rec size = function
      | Leaf -> 0
      | Node (t1, _, t2) -> 1 + size t1 + size t2
  end
