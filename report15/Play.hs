{-# LANGUAGE NamedFieldPuns #-}

module Play where

import Control.Applicative
import Control.Arrow
import Control.Monad

import Data.Bits
import Data.List (sortBy)
import Data.Maybe
import Data.Ord
import qualified Data.HashTable.IO as H

import System.CPUTime
import System.Random

import Board

type Score = Int
type Table = H.BasicHashTable (Bitboard, Bitboard) (Score, Score)

data Config = Config { depthTT :: Int
                     , depthMO :: Int
                     , eval :: Board -> Score
                     , sort :: [Move] -> IO [Move] }

midGameConf :: Config
midGameConf = Config { depthTT = 3
                     , depthMO = 3
                     , eval = evalBoard
                     , sort = return . sortMoves }

endGameConf :: Config
endGameConf = Config { depthTT = 8
                     , depthMO = 6
                     , eval = uncurry (-) . countPieces
                     , sort = return . sortMoves }

minBound' :: (Bounded a, Num a) => a
minBound' = negate maxBound

evalBoard :: Board -> Score
evalBoard board = diffMobility + diffStability * 10 + parity * 3
 where
  diffMobility  = (popCount $ mobility board) -
                  (popCount $ mobility $ swapPlayers board)
  diffStability = (popCount $ stability board) -
                  (popCount $ stability $ swapPlayers board)
  parity = countPieces' board `mod` 2

alphabeta :: Config -> Board -> Int -> Score -> Score -> Score
alphabeta conf@(Config {depthMO, eval}) board depth alpha beta
  | depth == 0 = eval board
  | mobility == 0 =
    if isGameOver board then
      evalBoard board
    else
      let board' = swapPlayers board
      in negate $ alphabeta conf board' depth (-beta) (-alpha)
  | depth < depthMO = go moves minBound'
  | otherwise = go (sortMoves moves) minBound' 
 where
  (mobility, moves) = mobilityAndValidMoves board
  go [] bestScore = bestScore
  go ((_, board) : moves) bestScore =
    let alpha' = max alpha bestScore
        board' = swapPlayers board
        score = negate $ alphabeta conf board' (depth-1) (-beta) (-alpha')
    in if beta <= score then score else go moves $ max bestScore score

alphabetaTT :: Config -> Board -> Int -> Score -> Score -> Table -> IO Score
alphabetaTT conf@(Config {depthTT, eval, sort}) board depth alpha beta table
  | depth < depthTT = return $ alphabeta conf board depth alpha beta
  | mobility == 0 =
    if isGameOver board then
      return $ eval board
    else do
      let board' = swapPlayers board
      score <- negate <$> alphabetaTT conf board' depth (-beta) (-alpha) table
      let score' =  if score >= beta then (score, maxBound)
                    else if score <= alpha then (minBound', score)
                    else (score, score)
      H.insert table (fst board) score'
      return score
  | otherwise = do
      cache <- H.lookup table $ fst board 
      case cache of
        Just (alpha', beta')
          | alpha' == beta' -> return alpha'
          | beta' <= alpha -> return beta'
          | alpha' >= beta -> return alpha'
          | otherwise -> do
              moves' <- sort moves 
              go (max alpha alpha') (min beta beta') moves' minBound'
        Nothing -> do 
          moves' <- sort moves 
          go alpha beta moves' minBound'
 where
  (mobility, moves) = mobilityAndValidMoves board
  go alpha beta [] bestScore = do
    let score = if bestScore > alpha then (bestScore, bestScore)
                                     else (minBound', bestScore)
    H.insert table (fst board) score
    return bestScore
  go alpha beta ((_, board) : moves) bestScore = do
    let alpha' = max alpha bestScore
        board' = swapPlayers board
    score <- negate <$> alphabetaTT conf board' (depth-1) (-beta) (-alpha') table
    if beta <= score then do
      H.insert table (fst board) (score, maxBound)
      return score
    else go alpha beta moves $ max bestScore score

sortMoves :: [Move] -> [Move]
sortMoves moves =
  let moves' = map (\move -> (move, evalBoard $ snd move)) moves
  in map fst $ sortBy (flip $ comparing snd) moves'

sortMovesTT :: Table -> [Move] -> IO [Move]
sortMovesTT table moves = do
  moves' <- forM moves $ \move -> do
    let board = snd move
    cache <- H.lookup table $ fst board
    return . (,) move $
      case cache of
        Just (alpha, beta)
          | alpha == beta -> alpha + cacheHit
          | alpha /= minBound' -> alpha + cacheHit
          | beta /= maxBound -> beta + cacheHit
          | otherwise -> cacheHit
        Nothing -> evalBoard board
  return $ map fst $ sortBy (flip $ comparing snd) moves'
 where
  cacheHit = 30

searchMove :: Config -> Board -> Int -> Move
searchMove conf board depth =
  fromJust $ go (sortMoves $ validMoves board) (Nothing, minBound')
 where
  go [] (bestMove, _) = bestMove
  go (move@(_, board) : moves)  best@(_, bestScore) =
    let board' = swapPlayers board
        score = negate $ alphabeta conf board' (depth-1) minBound' (-bestScore)
    in go moves $ if bestScore < score then (Just move, score) else best

searchMoveTT :: Config -> Board -> Int -> Table -> IO Move
searchMoveTT conf@(Config {sort}) board depth table = do
  moves <- sort $ validMoves board
  fromJust <$> go moves (Nothing, minBound')
 where
  go [] (bestMove, _) = return bestMove
  go (move@(_, board) : moves)  best@(_, bestScore) = do
    let board' = swapPlayers board
    score <- negate <$> alphabetaTT conf board' (depth-1) minBound' (-bestScore) table
    go moves $ if bestScore < score then (Just move, score) else best

idSearch :: Config -> Board -> Int -> Int -> Int -> IO (Move, Table) 
idSearch conf@(Config {depthTT}) board initialDepth maxDepth timeout = do
  start <- getCPUTime
  table <- H.newSized $ 2 ^ (initialDepth-depthTT) * 10
  move <- searchMoveTT conf board initialDepth table
  go start (initialDepth+1) table
 where
  go start depth oldTable = do
    table <- H.newSized $ 2 ^ (depth-depthTT) * 10
    let conf' = conf { sort = sortMovesTT oldTable }
    move <- searchMoveTT conf' board depth table
    current <- getCPUTime
    let elapsed = fromIntegral $ (current - start) `div` 10^9
    if elapsed > timeout || depth >= maxDepth then return (move, table)
                                              else go start (depth+1) table

play :: Board -> IO (Maybe Move)
play board
  | not (canMove board) = return Nothing 
  | blank == 60 = Just . (validMoves board !!) <$> randomRIO (0, 3)
  | blank <= 15 = return $ Just $ searchMove endGameConf board blank 
  | blank <= 18 = do
      table <- snd <$> idSearch midGameConf board 6 (blank-9) 250
      let conf = endGameConf { sort = sortMovesTT table }
      Just <$> (H.newSized (2 ^ (blank-10) * 10) >>= searchMoveTT conf board blank)
  | blank <= 30 = Just . fst <$> idSearch midGameConf board 6 14 750 
  | otherwise = Just . fst <$> idSearch midGameConf board 6 12 750
 where
  blank = 64 - countPieces' board
