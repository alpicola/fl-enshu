module Board where

import Control.Arrow
import Control.Monad

import Data.Bits
import Data.List
import Data.Char
import Data.Tuple
import Data.Word

type Bitboard = Word64
type Board = ((Bitboard, Bitboard), (Bitboard, Bitboard))
type Pos = Int
type Move = (Pos, Board)

infixl 8 .<<., .>>.

(.<<.) = unsafeShiftL
(.>>.) = unsafeShiftR

-- setBits :: Word64 -> [Int]
-- setBits x = go x 0
--  where
--   go x n | ctz == 64 = []
--          | otherwise = (ctz + n) : go (x .>>. shift) (n + shift) 
--    where
--     ctz = popCount $ (x .&. (-x)) - 1
--     shift = ctz + 1 
setBits :: Bits a => a -> [Int]
setBits x = filter (testBit x) [0..k]
 where k = bitSize x - 1

initialBoard :: Board
initialBoard = ((1 .<<. 35 .|. 1 .<<. 28, 1 .<<. 36 .|. 1 .<<. 27), (0, 0))

stability :: Board -> Bitboard
stability = fst . snd

mobility :: Board -> Bitboard
mobility = fst . mobilityAndValidMoves

validMoves :: Board -> [Move]
validMoves = snd . mobilityAndValidMoves

mobilityAndValidMoves :: Board -> (Bitboard, [Move])
mobilityAndValidMoves ((black, white), (stability, stability')) =
  (mobility, map genMove $ setBits mobility)
 where
  blank = complement $ black .|. white
  mask  = white .&. 0x7e7e7e7e7e7e7e7e
  h  = shifts' 1 black mask
  v  = shifts' 8 black white
  d1 = shifts' 7 black mask
  d2 = shifts' 9 black mask
  mobility = (or h .|. or v .|. or d1 .|. or d2) .&. blank
  or = uncurry (.|.)

  genMove pos = (pos, ((black', white'), (stability'', stability')))
   where
    black' = (black .|. 1 .<<. pos) `xor` flipped 
    white' = white `xor` flipped
    stability'' = spread stability black'
    x = 1 .<<. pos
    h'  = guard h $ shifts 1 x mask
    v'  = guard v $ shifts 8 x white
    d1' = guard d1 $ shifts 7 x mask
    d2' = guard d2 $ shifts 9 x mask
    flipped = or h' .|. or v' .|. or d1' .|. or d2'
    guard m = uncurry (***) $ guard' *** guard' $ swap m
     where
      guard' m x = if testBit m pos then x else 0

  shifts n init mask =
    let l1 = init .<<. n .&. mask
        l2 = (l1 .<<. n .&. mask) .|. l1
        l3 = (l2 .<<. n .&. mask) .|. l2
        l4 = (l3 .<<. n .&. mask) .|. l3
        l5 = (l4 .<<. n .&. mask) .|. l4
        l6 = (l5 .<<. n .&. mask) .|. l5
        r1 = init .>>. n .&. mask
        r2 = (r1 .>>. n .&. mask) .|. r1
        r3 = (r2 .>>. n .&. mask) .|. r2
        r4 = (r3 .>>. n .&. mask) .|. r3
        r5 = (r4 .>>. n .&. mask) .|. r4
        r6 = (r5 .>>. n .&. mask) .|. r5
    in (l6, r6)
  shifts' n init mask =
    (.<<. n) *** (.>>. n) $ shifts n init mask

  spread s mask = spread' . spread' . spread' .
                  spread' . spread' . spread' $ s
   where
    bh = 0x8181818181818181
    bv = 0xff000000000000ff
    bd = 0xff818181818181ff
    spread' s =
      let h  = s .<<. 1 .|. s .>>. 1 .|. bh
          v  = s .<<. 8 .|. s .>>. 8 .|. bv
          d1 = s .<<. 7 .|. s .>>. 7 .|. bd
          d2 = s .<<. 9 .|. s .>>. 9 .|. bd
      in (h .&. v .&. d1 .&. d2 .&. mask) .|. s

countPieces :: Board -> (Int, Int)
countPieces = (popCount *** popCount) . fst

countPieces' :: Board -> Int
countPieces' = popCount . uncurry (.|.) . fst

isGameOver :: Board -> Bool
isGameOver board = not $ canMove board || canMove (swapPlayers board)

canMove :: Board -> Bool
canMove board = mobility board /= 0

moveAt :: Pos -> Board -> Maybe Move
moveAt pos board = find ((pos ==) . fst) $ validMoves board

swapPlayers :: Board -> Board
swapPlayers = swap *** swap

showBoard :: Board -> String
showBoard ((black, white), _) = intercalate "\n" $ header : rows
 where
  header = "  " ++ intersperse ' ' ['a'..'h']
  rows = flip map [0..7] $ \y ->
    intersperse ' ' $ intToDigit (y+1) : map (piece . flip coord y) [0..7]
  coord x y = 63 - x - y * 8
  piece pos = if testBit black pos then 'x'
              else if testBit white pos then 'o'
              else ' '

showStability :: Board -> String
showStability = showBoard . swap

showBitboard :: Bitboard -> String
showBitboard = showBoard . flip (,) (0, 0) . flip (,) 0

readPos :: String -> Maybe Pos
readPos [x, y] = do
  x <- elemIndex x ['a'..'h'] `mplus` elemIndex x ['A'..'H']
  y <- elemIndex y ['1'..'8']
  return $ 63 - x - y * 8
readPos _ = Nothing

showPos :: Pos -> String
showPos pos = let pos' = 63 - pos
                  x = pos' `mod` 8
                  y = pos' `div` 8
              in  ['A'..'H'] !! x : intToDigit (y+1) : []
