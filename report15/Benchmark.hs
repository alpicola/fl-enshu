module Main where

import Data.List
import Data.Maybe

import Criterion.Main
import Criterion.Config

import Board
import Play (play)

slices :: Int -> [a] -> [[a]]
slices n xs = unfoldr f xs
 where
   f [] = Nothing
   f xs = Just $ splitAt n xs

applyMoves :: [String] -> Board -> Board
applyMoves [] board = board
applyMoves (move:moves) board =
  let board' = snd . fromJust $ readPos move >>= flip moveAt board
  in applyMoves moves $ swapPlayers board'

sampleMoves :: [String]
sampleMoves = slices 2 $ "F5D6C3D3C4F4F6F3E6E7D7G6D8C5C6C7"
                      ++ "C8B6A5B5B4C2G5A4E3A6D2E1B3E2A3A2"
                      ++ "F2F7H6F8B7E8A7A8B8H4H5G4H3B2G3F1"
                      ++ "G2H1G7H7D1C1A1B1H8G8G1H2"

config :: Config
config = defaultConfig { cfgSamples   = ljust 3
                       , cfgPerformGC = ljust True }

main = defaultMainWith config (return ())
  [ bgroup "opening"
    [ bench "opening-1" $ nfIO $ play $ applyMoves ["C4"] initialBoard 
    , bench "opening-2" $ nfIO $ play $ applyMoves (slices 2 "C4C3D3C5B4D2E2") initialBoard 
    , bench "opening-3" $ nfIO $ play $ applyMoves (slices 2 "C4E3F6E6F5C5F4G6F7D3") initialBoard
    ]
  , bgroup "middle"
    [ bench "middle-1" $ nfIO $ play $ applyMoves (take 20 sampleMoves) initialBoard
    , bench "middle-2" $ nfIO $ play $ applyMoves (take 30 sampleMoves) initialBoard
    , bench "middle-3" $ nfIO $ play $ applyMoves (take 40 sampleMoves) initialBoard
    ]
  , bgroup "end"
    [ bench "end-1" $ nfIO $ play $ applyMoves (take 42 sampleMoves) initialBoard
    , bench "end-2" $ nfIO $ play $ applyMoves (take 43 sampleMoves) initialBoard
    , bench "end-3" $ nfIO $ play $ applyMoves (take 45 sampleMoves) initialBoard
    ]
  ]
