module Main where

import Control.Applicative
import Control.Monad

import Data.Maybe

import System.IO

import Board
import Play

main = newGame

newGame :: IO ()
newGame = loop initialBoard True
 where
  loop :: Board -> Bool -> IO ()
  loop board isPlayer = do
    let (player, bot) = countPieces board
    putStrLn $ show player ++ ":" ++ show bot
    putStrLn $ showBoard board
    putStrLn ""
    if isGameOver board then do
      putStr "Game over. "
      case compare player bot of
        GT -> putStrLn "You win!"
        LT -> putStrLn "You lose!"
        EQ -> putStrLn "Tie."
    else
      if isPlayer then
        playerTurn board >>= flip loop False 
      else
        botTurn board >>= flip loop True

playerTurn :: Board -> IO Board
playerTurn board = do
  putStr "your turn: " >> hFlush stdout
  if canMove board then do
    input <- (readPos >=> flip moveAt board) <$> getLine
    case input of
      Just (_, board') -> return board'
      _ -> do
        putStrLn "Invalid input, try again."
        playerTurn board
  else do
    putStrLn "pass"
    return board

botTurn :: Board -> IO Board
botTurn board = do
  putStr "bot's turn: " >> hFlush stdout
  let board' = swapPlayers board
  move <- play board'
  case move of
    Just (pos, board'') -> do
      putStrLn $ showPos pos
      return $ swapPlayers board''
    _ -> do
      putStrLn "pass"
      return board
