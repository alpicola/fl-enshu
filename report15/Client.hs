{-# LANGUAGE NamedFieldPuns, RecordWildCards #-}

module Main where

import Control.Monad
import Control.Applicative hiding ((<|>), many)

import Network 
import System.IO
import System.Environment (getArgs) 
import System.Console.GetOpt

import Data.Char
import Data.List
import Data.Maybe
import Text.Parsec hiding (Empty)
import Text.Parsec.String

import Board
import Play (play)

data Config = Config { host :: HostName,
                       port :: PortNumber, 
                       playerName :: String,
                       verbose :: Bool,
                       helpMode :: Bool } 

data Color = Black | White deriving Eq
data Result = Win | Lose | Tie deriving Eq
data Command = Open String
             | End Result Int Int String 
             | Move (Maybe Pos)
             | Start Color String Int
             | Ack Int
             | Bye [(String, Int)]
             | Empty 

instance Show Color where
 show Black = "BLACK"
 show White = "WHITE"

instance Show Result where
 show Win = "WIN"
 show Lose = "LOSE"
 show Tie = "TIE"

instance Show Command where
  show (Open s) = "OPEN " ++ s
  show (End r i j s) = "END " ++ show r ++ " " ++ show i ++ " " ++ show j ++ " " ++ s
  show (Move m) = "MOVE " ++ maybe "PASS" showPos m
  show (Start c s i) = "START " ++ show c ++ " " ++ s ++ " " ++ show i 
  show (Ack t) = "ACK " ++ show t 
  show (Bye ss) = "BYE " ++ intercalate " " (map (\(s, i) -> s ++ " " ++ show i) ss)
  show Empty = ""

data PlayerState = PlayerState { name :: String
                               , color :: Color
                               , board :: Board
                               , timeout :: Int
                               , history :: [String] }

defaultConf :: Config
defaultConf = Config "localhost" 3000 "Anon." False False

options :: [OptDescr (Config -> Config)]
options =
  [ Option ['v'] ["verbose"]
           (NoArg $ \conf -> conf { verbose = True })
           "verbose mode"
  , Option ['H'] ["host"]
           (ReqArg (\s conf -> conf { host = s }) "HOST")
           "host name of a server" 
  , Option ['p'] ["port"]
           (ReqArg (\s conf -> conf { port = fromIntegral (read s :: Int) }) "PORT")
           "port number of a server"
  , Option ['n'] ["name"]
           (ReqArg (\s conf -> conf { playerName = s }) "NAME")
           "player name" 
  , Option ['h','?'] ["help"]
           (NoArg (\conf -> conf { helpMode = True }))
           "show this help"
  ]

usageMessage :: String
usageMessage = usageInfo header options 
 where
  header = "Usage: \n" 
           ++ "    reversi -H HOST -p PORT -n NAME ...\n" 

parseArgs :: [String] -> IO (Config, [String])
parseArgs args =
  case getOpt Permute options args of 
    (o, n, []) -> return (foldl (flip ($)) defaultConf o, n)
    (_, _, err) -> ioError (userError (concat err ++ usageMessage))

parseCommand :: String -> Either ParseError Command
parseCommand input =
  parse (command <* eof) "command" input 
 where
  command :: Parser Command
  command = Open <$ keyword "OPEN" <*> ident
        <|> End <$ keyword "END" <*> result <*> int <*> int <*> ident
        <|> Move <$ keyword "MOVE" <*> move
        <|> Start <$ keyword "START" <*> color <*> ident <*> int
        <|> Ack <$ keyword "ACK" <*> int
        <|> Bye <$ keyword "BYE" <*> many ((,) <$> ident <*> int)
        <|> pure Empty

  move :: Parser (Maybe Pos)
  move = (readPos .) . (\x y -> x:y:[]) <$> satisfy (\c -> 'A' <= c && c <= 'H')
                                        <*> satisfy (\c -> '1' <= c && c <= '8')
     <|> keyword "PASS" *> pure Nothing

  color :: Parser Color
  color = Black <$ keyword "BLACK"
      <|> White <$ keyword "WHITE"

  result :: Parser Result
  result = Win <$ keyword "WIN"
       <|> Lose <$ keyword "LOSE"
       <|> Tie <$ keyword "TIE"

  lexeme :: Parser a -> Parser a
  lexeme p = p <* spaces

  keyword :: String -> Parser String
  keyword = lexeme . string

  ident :: Parser String
  ident = lexeme $ many1 $ satisfy (not . isSpace)

  int :: Parser Int
  int = lexeme $ (read .) . (++) <$> option "" (string "-") <*> many1 digit

hGetCommand :: Handle -> IO Command 
hGetCommand h = do
  line <- hGetLine h
  putStrLn $ "Received: " ++ line 
  return $ either (error . show) id $ parseCommand line 

hGetCommand' :: Handle -> IO Command 
hGetCommand' h = do
  c <- hGetCommand h  
  case c of 
    Empty -> hGetCommand' h  
    _ -> return c

hPutCommand :: Handle -> Command -> IO ()
hPutCommand h c = do
  hPutStrLn h $ show c
  hFlush h
  putStrLn $ "Sent: " ++ show c

client :: Config -> IO ()
client (Config {..}) = do
  putStrLn $ "Connecting to " ++ host ++ " " ++ show port
  h <- connectTo host (PortNumber port) 
  putStrLn $ "Connection Ok."
  hPutCommand h $ Open playerName
  waitStart h playerName

waitStart :: Handle -> String -> IO ()
waitStart h pname = do
  c <- hGetCommand' h 
  case c of
    Bye scores -> do
      let len = maximum $ map (length . fst) scores
      putStr $ unlines $ flip map scores $ \(n, s) ->
        n ++ ":" ++ replicate (len + 1 - length n) ' ' ++ show s
    Start color opname time ->
      let s = PlayerState pname color initialBoard time []
      in if color == Black then performMove h s
                           else waitOpponentMove h s
    _ -> error $ "Invalid Command: " ++ show c
  
performMove :: Handle -> PlayerState -> IO ()
performMove h s@(PlayerState {..}) = do
  move <- play $ if color == Black then board else swapPlayers board
  let (c, s') = case move of
                  Just (pos, board') ->
                    let board'' = if color == Black then board' else swapPlayers board'
                    in (Just pos, s { board = board'', history = ('+' : showPos pos) : history })
                  _ -> (Nothing, s)
  hPutCommand h $ Move c
  c <- hGetCommand' h
  case c of 
    Ack t -> waitOpponentMove h s'  
    End r p o m -> procEnd h s r p o m 
    _ -> error $ "Invalid Command: " ++ show c 

waitOpponentMove :: Handle -> PlayerState -> IO ()
waitOpponentMove h s@(PlayerState {..}) = do
  c <- hGetCommand' h
  case c of 
    Move (Just pos) ->
      let board' = if color == White then snd $ fromJust $ moveAt pos board
                   else swapPlayers $ snd $ fromJust $ moveAt pos $ swapPlayers board
      in performMove h $ s { history = ('-' : showPos pos) : history, board = board' }
    Move Nothing -> performMove h s
    End r p o m -> procEnd h s r p o m 
    _ -> error $ "Invalid Command: " ++ show c 
           
procEnd :: Handle -> PlayerState -> Result -> Int -> Int -> String -> IO ()
procEnd h (PlayerState {..}) r p o message = do
  let result = case r of
                 Win -> "Win"
                 Lose -> "Lose"
                 Tie -> "Draw"
  putStrLn $ result ++ " (" ++ message ++ ")"
  putStrLn $ show p ++ ":" ++ show o
  putStrLn $ showBoard board
  putStrLn $ intercalate " " (reverse history)
  waitStart h name

main = do
  args <- getArgs 
  (conf, _) <- parseArgs args 
  if helpMode conf then putStrLn usageMessage 
                   else client conf
