type value = IntVal of int 
           | BoolVal of bool
           | FunVal of string * expr * env
           | RecFunVal of string * expr * env lazy_t
           | TupleVal of value list
           | ListVal of value list
and expr = Const of value
         | Var of string
         | Tuple of expr list
         | Cons of expr * expr
         | Nil
         | Add of expr * expr
         | Sub of expr * expr
         | Mul of expr * expr
         | Div of expr * expr
         | Lt of expr * expr
         | Eq of expr * expr
         | If of expr * expr * expr
         | Let of string * expr * expr
         | LetRec of (string * string * expr) list * expr
         | Fun of string * expr
         | App of expr * expr
         | Match of expr * (pattern * expr) list
and env = (string * value) list
and pattern = ConstPat of value
            | NilPat
            | ConsPat of pattern * pattern
            | TuplePat of pattern list
            | PatVar of string

type cmd = Expr of expr
         | Def of string * expr
         | DefRec of (string * string * expr) list

type ty = TyInt
        | TyBool
        | TyVar of int
        | TyFun of ty * ty
        | TyTuple of ty list
        | TyList of ty

exception Eval_error of string

exception Type_error
