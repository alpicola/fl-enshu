open Types

module IntSet = Set.Make (struct type t = int let compare = compare end)

let rec string_of_ty t =
  let diff xs ys = List.filter (fun n -> not (List.mem n ys)) xs in
  let union xs ys = xs @ diff ys xs in
  let rec iota start = function
    | 0 -> []
    | n -> start :: iota (start + 1) (n - 1) in
  let rec free_tyvar = function
    | TyInt | TyBool -> []
    | TyVar n -> [n]
    | TyFun (t1, t2) -> union (free_tyvar t1) (free_tyvar t2)
    | TyList t -> free_tyvar t
    | TyTuple ts ->
        List.fold_left (fun vars t ->
          union vars (free_tyvar t)) [] ts in
  let vars = free_tyvar t in
  let names = List.map char_of_int (iota 97 (List.length vars)) in
  let dict = List.combine vars names in
  let rec iter paren = function
    | TyInt -> "int"
    | TyBool -> "bool"
    | TyVar n -> 
        Printf.sprintf "'%c" (List.assoc n dict)
    | TyFun (t1, t2) ->
        let s = iter true t1 ^ " -> " ^ iter false t2 in
        if paren then "(" ^ s ^ ")" else s
    | TyList t -> iter true t ^ " list"
    | TyTuple ts ->
        let ss = List.map (iter false) ts in
        "(" ^ String.concat ", " ss ^ ")" in
  iter false t

let fresh_tyvar =
  let counter = ref 0 in
  fun () -> let n = !counter in counter := n + 1; TyVar n

let rec free_tyvar = function
  | TyInt | TyBool -> IntSet.empty
  | TyVar n -> IntSet.singleton n
  | TyFun (t1, t2) -> IntSet.union (free_tyvar t1) (free_tyvar t2)
  | TyList t -> free_tyvar t
  | TyTuple ts -> 
      List.fold_left (fun vars t ->
        IntSet.union vars (free_tyvar t)) IntSet.empty ts

let mono_ty t = (IntSet.empty, t)

let poly_ty t env =
  let vs = List.fold_left (fun fvs (id, (vs, t)) ->
    IntSet.union fvs (IntSet.diff (free_tyvar t) vs)) IntSet.empty env in
  (IntSet.diff (free_tyvar t) vs, t)

let eqs_of_subst s = List.map (fun (n, t) -> (TyVar n, t)) s

let rec substitute s = function
  | TyInt | TyBool as t -> t
  | TyVar n -> (try List.assoc n s with Not_found -> TyVar n)
  | TyFun (t1, t2) -> TyFun (substitute s t1, substitute s t2)
  | TyList t ->  TyList (substitute s t)
  | TyTuple ts ->  TyTuple (List.map (substitute s) ts)

let rec unify = function
  | [] -> []
  | (t1, t2) :: eqs when t1 = t2 -> unify eqs 
  | ((TyVar n, t) | (t, TyVar n)) :: eqs
    when not (IntSet.mem n (free_tyvar t)) ->
      let sub = substitute [(n, t)] in 
      let s = unify (List.map (fun (t1, t2) -> (sub t1, sub t2)) eqs) in
      (n, substitute s t) :: s
  | (TyFun (t1, t2), TyFun (t3, t4)) :: eqs ->
      unify ((t1, t3) :: (t2, t4) :: eqs)
  | (TyList t1, TyList t2) :: eqs ->
      unify ((t1, t2) :: eqs)
  | (TyTuple ts1, TyTuple ts2) :: eqs
    when List.length ts1 = List.length ts2 ->
      unify (List.combine ts1 ts2 @ eqs)
  | _ -> raise Type_error

let rec infer_pattern t = function
  | ConstPat (IntVal _) -> ([(t, TyInt)], [])
  | ConstPat (BoolVal _) -> ([(t, TyBool)], [])
  | NilPat -> ([(t, TyList (fresh_tyvar ()))], [])
  | ConsPat (pat, pat') ->
      let v = fresh_tyvar () in
      let (eqs1, bind1) = infer_pattern v pat in
      let (eqs2, bind2) = infer_pattern (TyList v) pat' in
      ((t, TyList v) :: (eqs1 @ eqs2), bind1 @ bind2)
  | TuplePat pats ->
      let (vs, eqs, bind) = List.fold_right (fun pat (vs, eqs1, bind1) ->
        let v = fresh_tyvar () in
        let (eqs2, bind2) = infer_pattern v pat in
        (v :: vs, eqs1 @ eqs2, bind1 @ bind2)) pats ([], [], []) in
      (((t, TyTuple vs) :: eqs), bind)
  | PatVar id -> ([], [(id, mono_ty t)])

let rec infer env = function 
  | Const (IntVal _) -> ([], TyInt)
  | Const (BoolVal _) -> ([], TyBool)
  | Var id ->
      (try
        let (ns, t) = List.assoc id env in
        let s = IntSet.fold (fun n s -> (n, fresh_tyvar ()) :: s) ns [] in 
        ([], substitute s t)
       with
         | Not_found -> failwith ("unbound variable: " ^ id))
  | Tuple es ->
      let (eqs, ts) = List.fold_right (fun e (eqs, ts) ->
        let (s, t) = infer env e in
        (eqs_of_subst s @ eqs, t :: ts)) es ([], []) in
      (unify eqs, TyTuple ts)
  | Nil -> ([], TyList (fresh_tyvar ()))
  | Cons (e1, e2) ->
      let (s1, t1) = infer env e1 in
      let (s2, t2) = infer env e2 in
      let eqs = eqs_of_subst (s1 @ s2) in
      let s3 = unify ((t2, TyList t1) :: eqs) in
      (s3, substitute s3 t2)
  | Add (e1, e2) | Sub (e1, e2) 
  | Mul (e1, e2) | Div (e1, e2) ->
      let (s1, t1) = infer env e1 in
      let (s2, t2) = infer env e2 in
      let eqs = eqs_of_subst (s1 @ s2) in
      (unify ((t1, TyInt) :: (t2, TyInt) :: eqs), TyInt)
  | Lt (e1, e2) | Eq (e1, e2) ->
      let (s1, t1) = infer env e1 in
      let (s2, t2) = infer env e2 in
      let eqs = eqs_of_subst (s1 @ s2) in
      (unify ((t1, TyInt) :: (t2, TyInt) :: eqs), TyBool)
  | If (e1, e2, e3) ->
      let (s1, t1) = infer env e1 in
      let (s2, t2) = infer env e2 in
      let (s3, t3) = infer env e3 in
      let eqs = eqs_of_subst (s1 @ s2 @ s3) in
      let s4 = unify ((t1, TyBool) :: (t2, t3) :: eqs) in
      (s4, substitute s4 t2)
  | Let (id, e1, e2) ->
      let (s1, t1) = infer env e1 in
      let sub = substitute s1 in
      let env2 = List.map (fun (id, (vs, t)) -> (id, (vs, sub t))) env in
      let tysc = poly_ty t1 env2 in
      let (s2, t2) = infer ((id, tysc) :: env) e2 in
      let s3 = unify (eqs_of_subst (s1 @ s2)) in
      (s3, substitute s3 t2)
  | LetRec (recs, e2) ->
      let vs = List.map (fun (id, _, _) -> (id, fresh_tyvar ())) recs in
      let env' = List.map (fun (id, v) -> (id, mono_ty v)) vs in
      let eqs = List.concat (List.map2 (fun (_, v1) (_, id, e) -> 
        let v2 = fresh_tyvar () in
        let (s, t) = infer ((id, mono_ty v2) :: env') e in
        (v1, TyFun (v2, t)) :: eqs_of_subst s) vs recs) in
      let s1 = unify eqs in
      let sub = substitute s1 in
      let env'' = List.map (fun (id, (vs, t)) -> (id, (vs, sub t))) env in
      let tyscs = List.map (fun (id, v) -> (id, poly_ty (sub v) env'')) vs in
      let (s2, t) = infer (tyscs @ env) e2 in
      let s3 = unify (eqs_of_subst (s1 @ s2)) in
      (s3, substitute s3 t)
  | Fun (id, e) ->
      let v = fresh_tyvar () in
      let (s, t) = infer ((id, mono_ty v) :: env) e in
      (s, TyFun (substitute s v, t))
  | App (e1, e2) ->
      let (s1, t1) = infer env e1 in
      let (s2, t2) = infer env e2 in
      let v = fresh_tyvar () in
      let s3 = unify ((t1, TyFun (t2, v)) :: eqs_of_subst (s1 @ s2)) in
      (s3, substitute s3 v)
  | Match (e, clauses) -> 
      let (s1, t1) = infer env e in
      let (eqs, t2 :: ts) = List.fold_right (fun (pat, e) (eqs1, ts) -> 
        let (eqs2, bind) = infer_pattern t1 pat in
        let (s, t) = infer (bind @ env) e in
        (eqs_of_subst s @ eqs1 @ eqs2, t :: ts)) clauses ([], []) in
      let s2 = unify (List.map (fun t -> (t2, t)) ts @ eqs @ eqs_of_subst s1) in
      (s2, substitute s2 t2)
