open Types
open Infer

let exec env = function
  | Expr e -> 
      (try
         let (_, t) = infer env e in
         (string_of_ty t, env)
       with 
         | Type_error -> ("Type error", env))
  | Def (id, e) ->
      let (s1, t1) = infer env e in
      let sub = substitute s1 in
      let env2 = List.map (fun (id, (vs, t)) -> (id, (vs, sub t))) env in
      let tysc = poly_ty t1 env2 in
      let mes = id ^ " : " ^ string_of_ty (snd tysc) in
      (mes, (id, tysc) :: env)
  | DefRec recs ->
      let vs = List.map (fun (id, _, _) -> (id, fresh_tyvar ())) recs in
      let env' = List.map (fun (id, v) -> (id, mono_ty v)) vs in
      let eqs = List.concat (List.map2 (fun (_, v1) (_, id, e) -> 
        let v2 = fresh_tyvar () in
        let (s, t) = infer ((id, mono_ty v2) :: env') e in
        (v1, TyFun (v2, t)) :: eqs_of_subst s) vs recs) in
      let s1 = unify eqs in
      let sub = substitute s1 in
      let env'' = List.map (fun (id, (vs, t)) -> (id, (vs, sub t))) env in
      let tyscs = List.map (fun (id, v) -> (id, poly_ty (sub v) env'')) vs in
      let mes = String.concat "\n" (List.map (fun (id, (_, t)) ->
        id ^ " : " ^ string_of_ty t) tyscs) in
      (mes, tyscs @ env)

let repl () =
  try
    let rec repl' env =
      print_string "# ";
      let input = read_line () in
      try 
        let lexbuf = Lexing.from_string input in
        match Parser.main Lexer.token lexbuf with
          | [] -> repl' env
          | cmd :: _ ->
            let (mes, env) = exec env cmd in
            print_endline mes;
            repl' env
      with
        | Failure mes ->
            print_endline mes;
            repl' env
        | Parsing.Parse_error ->
            print_endline "parser: syntax error";
            repl' env in
    repl' []
  with
    | End_of_file -> ()

let _ =
  if Array.length Sys.argv > 1
    then 
      try 
        let lexbuf = Lexing.from_channel (open_in Sys.argv.(1)) in
        let cmds = Parser.main Lexer.token lexbuf in
        List.fold_left (fun env cmd ->
          try
            let (mes, env) = exec env cmd in
            print_endline mes; env
          with
            | Failure mes ->
                print_endline mes; env
            | Parsing.Parse_error ->
                print_endline "parser: syntax error"; env) [] cmds; ()
      with
        | End_of_file -> ()
    else repl ()
