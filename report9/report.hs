{-# Language FlexibleInstances, GeneralizedNewtypeDeriving #-}

import Control.Applicative (Applicative, (<$>), (<*>), (*>), (<*), pure)
import Control.Monad.Identity
import Control.Monad.Error
import Control.Monad.Reader
import Control.Monad.State

import Data.Monoid
import Data.Maybe

import Text.Parsec
import Text.Parsec.String
import Text.Parsec.Expr
import Text.Parsec.Language
import qualified Text.Parsec.Token as P

main = interact $ unlines . map processInput . lines
 where
  processInput input =
    case parseExpr input of
      Left err -> show err
      Right expr ->
        let (result, count) = runEvalM $ evalExpr expr
        in either show show result ++ " (addition: " ++ show count ++ ")"

-- Types

data Expr = Const Value
          | Var String
          | Add Expr Expr
          | Sub Expr Expr
          | Mul Expr Expr
          | Div Expr Expr
          | Lt Expr Expr
          | If Expr Expr Expr
          | Let String Expr Expr
          deriving Show

data Value = Integer Integer
           | Bool Bool
type Env = [(String, Value)]

data EvalError = UnboundVariable String
               | TypeMismatch String
               | Default String

instance Show Value where
 show (Integer i) = show i
 show (Bool b) = show b

instance Show EvalError where
 show (UnboundVariable var) = "unbound variable: " ++ var
 show (TypeMismatch expected) = "expected: " ++ expected
 show (Default msg) = msg

instance Error EvalError where
  noMsg = Default "An error has occurred"
  strMsg = Default

newtype EvalM a = EvalM { unEvalM :: ErrorT EvalError (ReaderT Env (StateT Int Identity)) a }
 deriving (Functor, Applicative, Monad, MonadError EvalError, MonadState Int, MonadReader Env)

runEvalM :: EvalM a -> (Either EvalError a, Int)
runEvalM = runIdentity . flip runStateT 0 . flip runReaderT [] . runErrorT . unEvalM

-- Parser

resevedNames = ["if", "then", "else", "let", "in", "True", "False"]
lexer = P.makeTokenParser emptyDef { P.reservedNames = resevedNames }
reserved = P.reserved lexer
reservedOp = P.reservedOp lexer
ident = P.identifier lexer
integer = P.integer lexer
parens = P.parens lexer
unary op f = Prefix (reservedOp op *> pure f)
bin op f = Infix (reservedOp op *> pure f) AssocLeft

expr :: Parser Expr
expr = buildExpressionParser ops factor
   <|> If <$> (reserved "if" *> expr)
          <*> (reserved "then" *> expr) 
          <*> (reserved "else" *> expr) 
   <|> Let <$> (reserved "let" *> ident)
           <*> (reservedOp "=" *> expr) 
           <*> (reserved "in" *> expr) 
 where
  factor = Const <$> const
       <|> Var <$> ident
       <|> parens expr
  const = Integer <$> integer 
      <|> reserved "True" *> pure (Bool True) 
      <|> reserved "False" *> pure (Bool False)
  ops = [ [unary "-" (Sub (Const (Integer 0)))]
        , [bin "*" Mul, bin "/" Div]
        , [bin "+" Add, bin "-" Sub]
        , [bin "<" Lt] 
        ]

parseExpr :: String -> Either ParseError Expr
parseExpr = parse (expr <* eof) "test"

-- Evaluator

evalExpr :: Expr -> EvalM Value
evalExpr (Const val) = return val
evalExpr (Var var) = ask >>= maybe (throwError $ UnboundVariable var) return . lookup var
evalExpr (Add e e') = do val <- ((Integer .) . (+)) <$> (evalExpr e >>= fromIntegerValue)
                                                    <*> (evalExpr e' >>= fromIntegerValue)
                         modify (+1) >> return val
evalExpr (Sub e e') = ((Integer .) . (-)) <$> (evalExpr e >>= fromIntegerValue)
                                          <*> (evalExpr e' >>= fromIntegerValue)
evalExpr (Mul e e') = ((Integer .) . (*)) <$> (evalExpr e >>= fromIntegerValue)
                                          <*> (evalExpr e' >>= fromIntegerValue)
evalExpr (Div e e') = ((Integer .) . div) <$> (evalExpr e >>= fromIntegerValue)
                                          <*> (evalExpr e' >>= fromIntegerValue)
evalExpr (Lt e e') = ((Bool .) . (<)) <$> (evalExpr e >>= fromIntegerValue)
                                      <*> (evalExpr e' >>= fromIntegerValue)
evalExpr (If e e' e'') = do test <- evalExpr e >>= fromBoolValue
                            if test then evalExpr e' else evalExpr e''
evalExpr (Let var e e') = evalExpr e >>= flip local (evalExpr e') . (:) . (,) var

fromIntegerValue :: Value -> EvalM Integer
fromIntegerValue (Integer i) = return i
fromIntegerValue _ = throwError $ TypeMismatch "int"

fromBoolValue :: Value -> EvalM Bool
fromBoolValue (Bool b) = return b
fromBoolValue _ = throwError $ TypeMismatch "bool"
