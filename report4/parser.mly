%{
open Types
%}

%token <int> INT
%token <string> VAR

%token LPAR RPAR
%token PLUS MINUS TIMES DIV
%token LT EQ
%token AND OR NOT
%token IF THEN ELSE
%token LET IN
%token TRUE FALSE
%token DSEMICOL
%token EOF

%nonassoc ELSE IN
%left LT EQ
%left PLUS MINUS
%left TIMES DIV
%left AND OR
%nonassoc NOT

%start main
%type <Types.cmd list> main

%%

main:
    EOF { [] }
  | cmd main { $1 :: $2 }

cmd:
    expr DSEMICOL { Expr $1 }
  | LET VAR EQ expr DSEMICOL { Def ($2, $4) }

expr:
    INT   { Const (Int $1) }
  | TRUE  { Const (Bool true) }
  | FALSE { Const (Bool false) }
  | VAR { Var $1 }
  | LPAR expr RPAR { $2 }
  | expr PLUS expr  { Add ($1, $3) }
  | expr MINUS expr { Sub ($1, $3) }
  | expr TIMES expr { Mul ($1, $3) }
  | expr DIV expr   { Div ($1, $3) }
  | expr LT expr { Lt ($1, $3) }
  | expr EQ expr { Eq ($1, $3) }
  | expr AND expr { If ($1, $3, Const (Bool false)) }
  | expr OR expr  { If ($1, Const (Bool true), $3) }
  | NOT expr { If ($2, Const (Bool false), Const (Bool true))} 
  | IF expr THEN expr ELSE expr { If ($2, $4, $6) }
  | LET VAR EQ  expr IN expr { Let ($2, $4, $6) }
