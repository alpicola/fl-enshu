{
open Parser
}

let space = [' ' '\t' '\n' '\r']
let digit = ['0'-'9']
let alpha = ['A'-'Z' 'a'-'z' '_']

rule token = parse
    "(*" { comments 0 lexbuf }
  | "true"  { TRUE }
  | "false" { FALSE }
  | "not"   { NOT }
  | "if"    { IF }
  | "then"  { THEN }
  | "else"  { ELSE }
  | "let"   { LET }
  | "in"    { IN }
  | "&&" { AND }
  | "||" { OR }
  | ";;" { DSEMICOL }
  | '(' { LPAR }
  | ')' { RPAR }
  | '+' { PLUS }
  | '-' { MINUS }
  | '*' { TIMES }
  | '/' { DIV }
  | '<' { LT }
  | '=' { EQ }
  | alpha (alpha|digit)* as n { VAR n }
  | digit+ as n { INT (int_of_string n) }
  | space+ { token lexbuf }
  | eof { EOF }

and comments level = parse
    "*)" { if level = 0 then token lexbuf
           else comments (level-1) lexbuf }
  | "(*" { comments (level+1) lexbuf }
  | _ { comments level lexbuf }
  | eof { EOF }
