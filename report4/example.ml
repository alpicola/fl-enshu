let _ = 
  try 
    let lexbuf = Lexing.from_channel stdin in 
    let rec loop () = 
      let result = 
           ExampleParser.main ExampleLexer.token lexbuf in 
        print_int result; print_newline (); flush stdout;
        loop () in 
      loop ()
  with End_of_file -> exit 0 
