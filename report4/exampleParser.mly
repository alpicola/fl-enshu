%token <int> INT
%token MINUS PLUS TIMES DIV
%token LPAR RPAR 
%token EOL 

%start main 
%type <int> main

%% 

main: e EOL { $1 };

e:
   e1 { $1 }
 | e PLUS e1 { $1 + $3 }
 ;

e1:
   e2 { $1 }
 | e1 MINUS e2 { $1 - $3 }
 ;

e2:
   e3 { $1 }
 | e2 TIMES e3 { $1 * $3 }
 ;

e3:
   e4 { $1 }
 | e3 DIV e4 { $1 / $3 }
 ;

e4:
 | INT         { $1 }
 | LPAR e RPAR { $2 }
 | MINUS e4    { - $2 }
 ;
