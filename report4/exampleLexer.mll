{
  open ExampleParser 
}

let digit = ['0' - '9']

rule token = parse
  | [' ' '\t']* { token lexbuf }
  | '\n'        { EOL }
  | digit+ as n { INT (int_of_string n) }
  | '+'         { PLUS }
  | '-'         { MINUS }
  | '*'         { TIMES }
  | '/'         { DIV }
  | '('         { LPAR }
  | ')'         { RPAR }
  | eof         { raise End_of_file }
  | _           { failwith "Unrecognized Character" }
