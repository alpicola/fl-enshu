open Types

let rec eval env = function
  | Const v -> v
  | Var var ->
      (try
         List.assoc var env
       with
         | Not_found -> raise (Eval_error "unbound variable"))
  | Add (e, e') ->
      (match (eval env e, eval env e') with
         | (Int i, Int i') -> Int (i + i')
         | _ -> raise (Eval_error "type error"))
  | Sub (e, e') ->
      (match (eval env e, eval env e') with
         | (Int i, Int i') -> Int (i - i')
         | _ -> raise (Eval_error "type error"))
  | Mul (e, e') ->
      (match (eval env e, eval env e') with
         | (Int i, Int i') -> Int (i * i')
         | _ -> raise (Eval_error "type error"))
  | Div (e, e') ->
      (match (eval env e, eval env e') with
         | (Int i, Int i') -> Int (i / i')
         | _ -> raise (Eval_error "type error"))
  | Lt (e, e') ->
      (match (eval env e, eval env e') with
         | (Int i, Int i') -> Bool (i < i')
         | _ -> raise (Eval_error "type error"))
  | Eq  (e, e') ->
      (match (eval env e, eval env e') with
         | (Int i, Int i') -> Bool (i = i')
         | (Bool b, Bool b') -> Bool (b = b')
         | _ -> raise (Eval_error "type error"))
  | If (e, e', e'') ->
      (match eval env e with
         | Bool true -> eval env e'
         | Bool false -> eval env e''
         | _ -> raise (Eval_error "type error"))
  | Let (var, e, e') ->
      eval ((var, eval env e) :: env) e'

let exec env = function
  | Expr expr -> 
      (try
         let result = eval env expr in
         (string_of_val result, env)
       with 
         | Eval_error mes -> ("eval: " ^ mes, env))
  | Def (var, expr) ->
      (try
         let result = eval env expr in
         (var ^ " = " ^ string_of_val result , (var, result) :: env)
       with 
         | Eval_error mes -> ("eval: " ^ mes, env))
