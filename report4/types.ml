type value = Int of int 
           | Bool of bool

type expr = Const of value
          | Var of string
          | Add of expr * expr
          | Sub of expr * expr
          | Mul of expr * expr
          | Div of expr * expr
          | Eq of expr * expr
          | Lt of expr * expr
          | If of expr * expr * expr
          | Let of string * expr * expr

type cmd = Expr of expr
         | Def of string * expr

exception Eval_error of string

let string_of_val = function
  | Int i -> string_of_int i
  | Bool b -> string_of_bool b
